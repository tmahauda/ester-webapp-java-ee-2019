package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.ResultatRef;
import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class ResultatRefDAOTest extends ModelDAOTest<ResultatRef> {

    public ResultatRefDAOTest() {
        super(ResultatRefDAO.getInstance(), ResultatRef.class);
    }

    @Override
    protected List<ResultatRef> getModels() {
        return DataEster.getListResultatRef();
    }
}