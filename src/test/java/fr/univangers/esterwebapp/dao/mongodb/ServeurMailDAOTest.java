package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.ServeurMail;

import java.util.List;

public class ServeurMailDAOTest extends ModelDAOTest<ServeurMail> {

    public ServeurMailDAOTest() {
        super(ServeurMailDAO.getInstance(), ServeurMail.class);
    }

    @Override
    protected List<ServeurMail> getModels() {
        return DataEster.getListServeursMail();
    }
}