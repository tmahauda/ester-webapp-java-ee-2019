package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.operation.OperationModelTest;

public abstract class ModelDAOTest<T extends Model> extends OperationModelTest<T> {

    protected ModelDAOTest(ModelDAO<T> modelDAO, Class<T> modelClass) {
        super(modelDAO, modelClass);
    }
}