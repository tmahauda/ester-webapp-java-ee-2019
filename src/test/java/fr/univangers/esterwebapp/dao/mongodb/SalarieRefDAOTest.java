package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.SalarieRef;
import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class SalarieRefDAOTest extends UtilisateurDAOTest<SalarieRef> {

    public SalarieRefDAOTest() {
        super(SalarieRefDAO.getInstance(), SalarieRef.class);
    }

    @Override
    protected List<SalarieRef> getModels() {
        return DataEster.getListSalarieRef();
    }
}