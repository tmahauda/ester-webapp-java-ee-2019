package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Entreprise;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.proposition.Diplome;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Age;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Sexe;
import org.junit.jupiter.api.Test;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class UtilisateurEsterDAOTest extends ConnexionDAOTest<UtilisateurEster> {

    public UtilisateurEsterDAOTest() {
        super(UtilisateurEsterDAO.getInstance(), UtilisateurEster.class);
    }

    @Override
    protected List<UtilisateurEster> getModels() {
        return DataEster.getListUtilisateurEster();
    }

    /**
     * Ajout d'un nouveau salarié qui n'existe pas et d'une entreprise qui n'existe pas dans la BD partagé entre 1 nouveau utilisateurEster
     */
    @Test
    public void testAddNewSalarieNewEntrepriseForNewEster() {
        Entreprise entreprise = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com",
                "Entreprise@True!ESTER05", true);

        Salarie salarie1 = new Salarie("SalarieTrueSALARIE09", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        Salarie salarie2 = new Salarie("SalarieTrueSALARIE10", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        boolean test = UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        assertTrue(test);

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertEquals(this.models.get(0), utilisateurEsterBD);

        int size = utilisateurEsterBD.getSalariesCrees().size();
        assertEquals(2, size);

        Salarie salarieBD1 = utilisateurEsterBD.getSalariesCrees().stream().filter(s -> s.getIdentifiant().equals("SalarieTrueSALARIE09")).findFirst().get();
        Salarie salarieBD2 = utilisateurEsterBD.getSalariesCrees().stream().filter(s -> s.getIdentifiant().equals("SalarieTrueSALARIE10")).findFirst().get();
        assertNotNull(salarieBD1);
        assertNotNull(salarieBD2);
        assertEquals(salarie1, salarieBD1);
        assertEquals(salarie2, salarieBD2);

        Entreprise entrepriseBD1 = salarieBD1.getEntreprise();
        Entreprise entrepriseBD2 = salarieBD2.getEntreprise();
        assertNotNull(entrepriseBD1);
        assertNotNull(entrepriseBD2);
        assertEquals(entreprise, entrepriseBD1);
        assertEquals(entreprise, entrepriseBD2);
        assertEquals(entrepriseBD1, entrepriseBD2);
    }

    /**
     * Ajout d'un nouveau salarié qui n'existe pas et d'une entreprise qui n'existe pas dans la BD partagé entre 1 utilisateurEster existant dans la BD
     */
    @Test
    public void testAddNewSalarieNewEntrepriseForOldEster() {
        Entreprise entreprise = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com",
                "Entreprise@True!ESTER05", true);

        Salarie salarie = new Salarie("SalarieTrueSALARIE09", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        UtilisateurEsterDAO.getInstance().save(this.models.get(0));

        boolean test = UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        assertTrue(test);

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertEquals(this.models.get(0), utilisateurEsterBD);

        int size = utilisateurEsterBD.getSalariesCrees().size();
        assertEquals(1, size);

        Salarie salarieBD = utilisateurEsterBD.getSalariesCrees().toArray(new Salarie[size])[0];
        assertNotNull(salarieBD);
        assertEquals(salarie, salarieBD);

        Entreprise entrepriseBD = salarieBD.getEntreprise();
        assertNotNull(entrepriseBD);
        assertEquals(entreprise, entrepriseBD);
    }

    /**
     * Ajout d'un nouveau salarié qui n'existe pas et d'une entreprise qui existe dans la BD partagé entre 1 nouveau utilisateurEster
     */
    @Test
    public void testAddNewSalarieOldEntrepriseForNewEster() {
        Entreprise entreprise = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com",
                "Entreprise@True!ESTER05", true);

        Salarie salarie = new Salarie("SalarieTrueSALARIE09", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        EntrepriseDAO.getInstance().save(entreprise);

        boolean test = UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        assertTrue(test);

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertEquals(this.models.get(0), utilisateurEsterBD);

        int size = utilisateurEsterBD.getSalariesCrees().size();
        assertEquals(1, size);

        Salarie salarieBD = utilisateurEsterBD.getSalariesCrees().toArray(new Salarie[size])[0];
        assertNotNull(salarieBD);
        assertEquals(salarie, salarieBD);

        Entreprise entrepriseBD = salarieBD.getEntreprise();
        assertNotNull(entrepriseBD);
        assertEquals(entreprise, entrepriseBD);
    }

    /**
     * Ajout d'un nouveau salarié qui n'existe pas et d'une entreprise qui existe dans la BD partagé entre 1 utilisateurEster existant dans la BD
     */
    @Test
    public void testAddNewSalarieOldEntrepriseForOldEster1() {
        Entreprise entreprise = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com",
                "Entreprise@True!ESTER05", true);

        Salarie salarie = new Salarie("SalarieTrueSALARIE09", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        EntrepriseDAO.getInstance().save(entreprise);

        boolean test = UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        assertTrue(test);

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertEquals(this.models.get(0), utilisateurEsterBD);

        int size = utilisateurEsterBD.getSalariesCrees().size();
        assertEquals(1, size);

        Salarie salarieBD = utilisateurEsterBD.getSalariesCrees().toArray(new Salarie[size])[0];
        assertNotNull(salarieBD);
        assertEquals(salarie, salarieBD);

        Entreprise entrepriseBD = salarieBD.getEntreprise();
        assertNotNull(entrepriseBD);
        assertEquals(entreprise, entrepriseBD);
    }

    /**
     * Ajout d'un nouveau salarié qui n'existe pas et d'une entreprise qui existe dans la BD partagé entre 1 utilisateurEster existant dans la BD
     */
    @Test
    public void testAddNewSalarieOldEntrepriseForOldEster2() {
        Entreprise entreprise = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com",
                "Entreprise@True!ESTER05", true);

        Salarie salarie = new Salarie("SalarieTrueSALARIE09", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        EntrepriseDAO.getInstance().save(entreprise);
        UtilisateurEsterDAO.getInstance().save(this.models.get(0));

        boolean test = UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        assertTrue(test);

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertEquals(this.models.get(0), utilisateurEsterBD);

        int size = utilisateurEsterBD.getSalariesCrees().size();
        assertEquals(1, size);

        Salarie salarieBD = utilisateurEsterBD.getSalariesCrees().toArray(new Salarie[size])[0];
        assertNotNull(salarieBD);
        assertEquals(salarie, salarieBD);

        Entreprise entrepriseBD = salarieBD.getEntreprise();
        assertNotNull(entrepriseBD);
        assertEquals(entreprise, entrepriseBD);
    }

    /**
     * Ajout d'un salarié qui existe et d'une entreprise qui existe dans la BD partagé entre 1 nouveau utilisateurEster
     */
    @Test
    public void testAddOldSalarieNewEntrepriseForNewEster() {
        Entreprise entreprise = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com",
                "Entreprise@True!ESTER05", true);

        Salarie salarie = new Salarie("SalarieTrueSALARIE09", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        EntrepriseDAO.getInstance().save(entreprise);

        boolean test = UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        assertTrue(test);

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertEquals(this.models.get(0), utilisateurEsterBD);

        int size = utilisateurEsterBD.getSalariesCrees().size();
        assertEquals(1, size);

        Salarie salarieBD = utilisateurEsterBD.getSalariesCrees().toArray(new Salarie[size])[0];
        assertNotNull(salarieBD);
        assertEquals(salarie, salarieBD);

        Entreprise entrepriseBD = salarieBD.getEntreprise();
        assertNotNull(entrepriseBD);
        assertEquals(entreprise, entrepriseBD);
    }

    /**
     * Ajout d'un salarié qui existe et d'une entreprise qui existe dans la BD partagé entre 1 utilisateurEster existant
     */
    @Test
    public void testAddOldSalarieNewEntrepriseForOldEster1() {
        Entreprise entreprise = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com",
                "Entreprise@True!ESTER05", true);

        Salarie salarie = new Salarie("SalarieTrueSALARIE09", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        SalarieDAO.getInstance().save(salarie);

        boolean test = UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        assertTrue(test);

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertEquals(this.models.get(0), utilisateurEsterBD);

        int size = utilisateurEsterBD.getSalariesCrees().size();
        assertEquals(1, size);

        Salarie salarieBD = utilisateurEsterBD.getSalariesCrees().toArray(new Salarie[size])[0];
        assertNotNull(salarieBD);
        assertEquals(salarie, salarieBD);

        Entreprise entrepriseBD = salarieBD.getEntreprise();
        assertNotNull(entrepriseBD);
        assertEquals(entreprise, entrepriseBD);
    }

    /**
     * Ajout d'un salarié qui existe et d'une entreprise qui existe dans la BD partagé entre 1 utilisateurEster existant
     */
    @Test
    public void testAddOldSalarieNewEntrepriseForOldEster2() {
        Entreprise entreprise = new Entreprise("EntrepriseTrueENTREPRISE05", "ENTREPRISE-CINQ", "entreprisetrue.ester05@yahoo.com",
                "Entreprise@True!ESTER05", true);

        Salarie salarie = new Salarie("SalarieTrueSALARIE09", true, Sexe.HOMME, Age.ANS_55_59, Diplome.PAS_DE_DIPLOME, null,
                entreprise, this.models.get(0), 0, 0);

        SalarieDAO.getInstance().save(salarie);
        UtilisateurEsterDAO.getInstance().save(this.models.get(0));

        boolean test = UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        assertTrue(test);

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertEquals(this.models.get(0), utilisateurEsterBD);

        int size = utilisateurEsterBD.getSalariesCrees().size();
        assertEquals(1, size);

        Salarie salarieBD = utilisateurEsterBD.getSalariesCrees().toArray(new Salarie[size])[0];
        assertNotNull(salarieBD);
        assertEquals(salarie, salarieBD);

        Entreprise entrepriseBD = salarieBD.getEntreprise();
        assertNotNull(entrepriseBD);
        assertEquals(entreprise, entrepriseBD);
    }

    /**
     * Récupération d'un utilisateur ester avec ses questionnaires dont il a crées
     */
    @Test
    public void testReadQuestionnaireCreateurCrees() {
        Questionnaire questionnaire1 = new Questionnaire(1, "Q1", "Questionnaire 1", new Date(), new Date(), this.models.get(0));
        Questionnaire questionnaire2 = new Questionnaire(2, "Q2", "Questionnaire 2", new Date(), new Date(), this.models.get(0));
        UtilisateurEsterDAO.getInstance().save(this.models.get(0));

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertFalse(utilisateurEsterBD.getQuestionnairesCrees().isEmpty());
        assertEquals(2, utilisateurEsterBD.getQuestionnairesCrees().size());

        Questionnaire questionnaire1BD = utilisateurEsterBD.getQuestionnairesCrees().stream().filter(q -> q.getUuid().toString().equals(questionnaire1.getUuid().toString())).findFirst().get();
        Questionnaire questionnaire2BD = utilisateurEsterBD.getQuestionnairesCrees().stream().filter(q ->q.getUuid().toString().equals(questionnaire2.getUuid().toString())).findFirst().get();
        assertNotNull(questionnaire1BD);
        assertNotNull(questionnaire2BD);

        assertEquals(questionnaire1, questionnaire1BD);
        assertEquals(questionnaire2, questionnaire2BD);
    }

    /**
     * Récupération d'un utilisateur ester avec ses questionnaires dont il a acces
     */
    @Test
    public void testReadQuestionnaireCreateurAcces() {
        Questionnaire questionnaire1 = new Questionnaire(1, "Q1", "Questionnaire 1", new Date(), new Date(), this.models.get(0));
        Questionnaire questionnaire2 = new Questionnaire(2, "Q2", "Questionnaire 2", new Date(), new Date(), this.models.get(0));

        UtilisateurEsterDAO.getInstance().save(this.models.get(0));

        UtilisateurEster utilisateurEsterBD = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        assertNotNull(utilisateurEsterBD);
        assertFalse(utilisateurEsterBD.getQuestionnairesAcces().isEmpty());
        assertEquals(2, utilisateurEsterBD.getQuestionnairesAcces().size());

        Questionnaire questionnaire1BD = utilisateurEsterBD.getQuestionnairesAcces().stream().filter(q -> q.getUuid().toString().equals(questionnaire1.getUuid().toString())).findFirst().get();
        Questionnaire questionnaire2BD = utilisateurEsterBD.getQuestionnairesAcces().stream().filter(q ->q.getUuid().toString().equals(questionnaire2.getUuid().toString())).findFirst().get();
        assertNotNull(questionnaire1BD);
        assertNotNull(questionnaire2BD);

        assertEquals(questionnaire1, questionnaire1BD);
        assertEquals(questionnaire2, questionnaire2BD);
    }

    /**
     * Récupération d'un utilisateur ester avec ses questionnaires dont il a acces mais il n'est pas créateur
     */
    @Test
    public void testReadQuestionnaireNonCreateurAcces() {
        Questionnaire questionnaire1 = new Questionnaire(1, "Q1", "Questionnaire 1", new Date(), new Date(), this.models.get(0));
        Questionnaire questionnaire2 = new Questionnaire(2, "Q2", "Questionnaire 2", new Date(), new Date(), this.models.get(0));

        this.models.get(1).getQuestionnairesAcces().add(questionnaire1);
        this.models.get(1).getQuestionnairesAcces().add(questionnaire2);
        questionnaire1.getModificateursQuestion().add(this.models.get(1));
        questionnaire2.getModificateursQuestion().add(this.models.get(1));

        UtilisateurEsterDAO.getInstance().save(this.models.get(0));
        UtilisateurEsterDAO.getInstance().save(this.models.get(1));

        UtilisateurEster utilisateurEsterBD0 = UtilisateurEsterDAO.getInstance().readById(this.models.get(0).getUuid());
        UtilisateurEster utilisateurEsterBD1 = UtilisateurEsterDAO.getInstance().readById(this.models.get(1).getUuid());
        assertNotNull(utilisateurEsterBD0);
        assertNotNull(utilisateurEsterBD1);

        //L'ester 0 a des questionnaires en access
        assertFalse(utilisateurEsterBD0.getQuestionnairesAcces().isEmpty());
        assertEquals(2, utilisateurEsterBD0.getQuestionnairesAcces().size());

        //L'ester 0 a crées des questionnaires
        assertFalse(utilisateurEsterBD0.getQuestionnairesCrees().isEmpty());
        assertEquals(2, utilisateurEsterBD0.getQuestionnairesCrees().size());

        //L'ester 1 a des questionnaires en access
        //assertFalse(utilisateurEsterBD1.getQuestionnairesAcces().isEmpty());
        assertEquals(2, utilisateurEsterBD1.getQuestionnairesAcces().size());

        //L'ester 1 n'a crée aucun questionnaire
        assertTrue(utilisateurEsterBD1.getQuestionnairesCrees().isEmpty());
        assertEquals(0, utilisateurEsterBD1.getQuestionnairesCrees().size());

        Questionnaire questionnaire1BD = utilisateurEsterBD0.getQuestionnairesAcces().stream().filter(q -> q.getUuid().toString().equals(questionnaire1.getUuid().toString())).findFirst().get();
        Questionnaire questionnaire2BD = utilisateurEsterBD0.getQuestionnairesAcces().stream().filter(q ->q.getUuid().toString().equals(questionnaire2.getUuid().toString())).findFirst().get();
        assertNotNull(questionnaire1BD);
        assertNotNull(questionnaire2BD);

        assertEquals(questionnaire1, questionnaire1BD);
        assertEquals(questionnaire2, questionnaire2BD);
    }

}