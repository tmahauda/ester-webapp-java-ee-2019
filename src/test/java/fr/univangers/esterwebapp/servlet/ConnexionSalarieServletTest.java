package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.SalarieService;
import fr.univangers.esterwebapp.model.Utilisateur;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.List;

public class ConnexionSalarieServletTest extends ConnexionUtilisateurServletTest<Salarie> {

    private static final String TYPE = ConnexionServlet.TYPE_VALUE_SALARIE;
    private static final Role ROLE = Role.SALARIE;
    private static final String ID = "SalarieTrueSALARIE01";
    private static final String PASSWORD = null;

    public ConnexionSalarieServletTest() {
        super(TYPE, ROLE, ID, ID, PASSWORD, SalarieService.getInstance());
    }

    @Override
    protected List<Salarie> getModels() {
        return DataEster.getListSalarie();
    }

    /**
     * Test avec
     * - Type correct
     * - Identifiant correct
     * - mot de passe incorrect
     */
    @Override
    @Test
    protected void testDoPostUtilisateurEsterTypeOKIdOKPasswordKO() {
        Utilisateur user = SalarieService.getInstance().readByEmailOrId(ID);
        assertNotNull(user);
        this.testDoPost(TYPE, ID, "bidon",
                Servlet.ATT_MSG_SUCCESS.getType(), user, ROLE);
    }

    /**
     * Test avec
     * - Type correct
     * - Identifiant correct
     * - mot de passe incorrect
     */
    @Override
    @Test
    protected void testDoPostUtilisateurEsterTypeOKEmailOKPasswordKO() {
        Utilisateur user = SalarieService.getInstance().readByEmailOrId(ID);
        assertNotNull(user);
        this.testDoPost(TYPE, ID, "bidon",
                Servlet.ATT_MSG_SUCCESS.getType(), user, ROLE);
    }
}
