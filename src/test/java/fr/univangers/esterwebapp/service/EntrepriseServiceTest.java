package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Entreprise;
import java.util.List;

public class EntrepriseServiceTest extends ConnexionServiceTest<Entreprise> {

    protected EntrepriseServiceTest() {
        super(EntrepriseService.getInstance(), Entreprise.class);
    }

    @Override
    protected List<Entreprise> getModels() {
        return DataEster.getListEntreprise();
    }
}