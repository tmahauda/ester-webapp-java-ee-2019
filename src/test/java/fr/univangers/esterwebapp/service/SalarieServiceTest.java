package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.Salarie;
import java.util.List;

public class SalarieServiceTest extends UtilisateurServiceTest<Salarie> {

    public SalarieServiceTest() {
        super(SalarieService.getInstance(), Salarie.class);
    }

    @Override
    protected List<Salarie> getModels() {
        return DataEster.getListSalarie();
    }
}