package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.ResultatRef;
import fr.univangers.esterwebapp.DataEster;

import java.util.List;

public class ResultatServiceRefTest extends ModelServiceTest<ResultatRef> {

    protected ResultatServiceRefTest() {
        super(ResultatRefService.getInstance(), ResultatRef.class);
    }

    @Override
    protected List<ResultatRef> getModels() {
        return DataEster.getListResultatRef();
    }
}
