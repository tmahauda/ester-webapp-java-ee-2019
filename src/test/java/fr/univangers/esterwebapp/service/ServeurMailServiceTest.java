package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.DataEster;
import fr.univangers.esterwebapp.model.ServeurMail;

import java.util.List;

public class ServeurMailServiceTest extends ModelServiceTest<ServeurMail> {

    protected ServeurMailServiceTest() {
        super(ServeurMailService.getInstance(), ServeurMail.class);
    }

    @Override
    protected List<ServeurMail> getModels() {
        return DataEster.getListServeursMail();
    }
}