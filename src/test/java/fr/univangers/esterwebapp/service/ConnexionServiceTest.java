package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.Connexion;
import fr.univangers.esterwebapp.operation.OperationConnexionTest;

public abstract class ConnexionServiceTest<T extends Connexion> extends OperationConnexionTest<T> {

    protected ConnexionServiceTest(ConnexionService<T> connexionService, Class<T> connexionClass) {
        super(connexionService, connexionClass);
    }
}