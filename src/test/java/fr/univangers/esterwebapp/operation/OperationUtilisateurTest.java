package fr.univangers.esterwebapp.operation;

import fr.univangers.esterwebapp.model.Utilisateur;

public abstract class OperationUtilisateurTest<T extends Utilisateur> extends OperationModelTest<T> {

    protected OperationUtilisateur<T> operationUtilisateur;

    protected OperationUtilisateurTest(OperationUtilisateur<T> operationUtilisateur, Class<T> classModel) {
        super(operationUtilisateur, classModel);
        this.operationUtilisateur = operationUtilisateur;
    }
}