package fr.univangers.esterwebapp;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.operation.OperationModel;
import fr.univangers.esterwebapp.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;

/**
 * Test abstrait qui va paramétrer la base de test
 */
public abstract class TestEster<T extends Model> {

    private static Logger log = Logger.getLogger(TestEster.class);

    /**
     * Les tests vont portés principalement sur l'état des models
     */
    protected List<T> models;

    /**
     * Constructeur qui va initialiser la base de test
     */
    protected TestEster() {
        //On désactive les logs durant les tests pour aller plus rapide
        HibernateUtil.activateLog(false);
        log.info("Log désactivé");
    }

    /**
     * Méthode qui va nettoyer la base de test au début de chaque cas de test
     */
    @BeforeEach
    protected void setUp() {
        HibernateUtil.deleteDataBaseTest();
        HibernateUtil.openTest();
        this.models = this.getModels();
    }

    /**
     * Méthode qui va nettoyer la base de test à la fin de chaque cas de test
     */
    @AfterEach
    protected void tearDown() {
        HibernateUtil.close();
        HibernateUtil.deleteDataBaseTest();
        this.models = null;
    }

    /**
     * Peupler la base de données avec des données
     */
    protected void populateDataBase(OperationModel<T> operationModel) {
        HibernateUtil.populateDataBase(this.models, operationModel);
    }

    protected abstract List<T> getModels();
}