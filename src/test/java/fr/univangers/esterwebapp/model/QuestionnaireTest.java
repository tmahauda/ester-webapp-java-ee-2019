package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class QuestionnaireTest extends ModelTest<Questionnaire> {

    @Override
    protected List<Questionnaire> getModels() {
        return DataEster.getListQuestionnaire();
    }
}
