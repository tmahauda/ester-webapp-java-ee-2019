package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class UtilisateurEsterTest extends ModelTest<UtilisateurEster> {

    @Override
    protected List<UtilisateurEster> getModels() {
        return DataEster.getListUtilisateurEster();
    }
}
