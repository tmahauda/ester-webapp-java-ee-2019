package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class ResultatRefTest extends ModelTest<ResultatRef> {

    @Override
    protected List<ResultatRef> getModels() {
        return DataEster.getListResultatRef();
    }
}