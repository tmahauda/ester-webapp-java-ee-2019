package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class EntrepriseTest extends ModelTest<Entreprise> {

    @Override
    protected List<Entreprise> getModels() {
        return DataEster.getListEntreprise();
    }
}
