package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class SalarieTest extends ModelTest<Salarie> {

    @Override
    protected List<Salarie> getModels() {
        return DataEster.getListSalarie();
    }
}
