package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.DataEster;
import java.util.List;

public class SalarieRefTest extends ModelTest<SalarieRef> {

    @Override
    protected List<SalarieRef> getModels() {
        return DataEster.getListSalarieRef();
    }
}
