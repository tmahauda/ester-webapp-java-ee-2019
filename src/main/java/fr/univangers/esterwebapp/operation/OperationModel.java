package fr.univangers.esterwebapp.operation;

import fr.univangers.esterwebapp.model.Model;

import java.util.Collection;
import java.util.UUID;

/**
 * Opérations CRUD permettant de gérer l'état d'une entité T dans une base de données
 * @param <T> l'entité
 */
public interface OperationModel<T extends Model> {

    /**
     * Enregistrer une entité dans la BD
     * @param entity à enregistrer dans la BD
     * @return vrai si enregistré. Faux dans le cas contraire
     */
    public boolean create(T entity);

    /**
     * Collections de toutes les entités trouvés
     * @return Une collection d'entité
     */
    public Collection<T> read();

    /**
     * Retourne l'entité enregistré en dernier dans la BD
     * @return l'entité enregistré en dernier dans la BD
     */
    public T readLast();

    /**
     * Retourne l'entité enregistré en premier dans la BD
     * @return l'entité enregistré en premier dans la BD
     */
    public T readFirst();

    /**
     * Trouver une entité dans la BD par son uuid
     * @param uuid l'identifiant unique de l'entité
     * @return l'entité
     */
    public T readById(UUID uuid);

    /**
     * Trouver une entité dans la BD par son id
     * @param id l'identifiant unique de l'utilisateur
     * @return l'utilisateur
     */
    public T readById(String id);

    /**
     * Mise à jour d'une entité existante dans la BD
     * @param entity l'entité à mettre à jour
     * @return vrai si mise à jour. Faux dans le cas contraire
     */
    public boolean update(T entity);

    /**
     * Créer ou mise à jour d'une entite dans la BD
     * @param entity l'entité à créer ou mettre à jour
     * @return vrai si mise à jour ou créer. Faux dans le cas contraire
     */
    public boolean save(T entity);

    /**
     * Suppression d'une entité existante dans la BD
     * @param entity l'entité à supprimer
     * @return vrai si supprimé. Faux dans le cas contraire
     */
    public boolean delete(T entity);

    /**
     * Suppression de toutes les entités de la BD
     * @return vrai si tout supprimé. Faux dans le cas contraire
     */
    public boolean delete();
}