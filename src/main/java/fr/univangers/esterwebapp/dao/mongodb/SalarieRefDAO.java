package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.SalarieRef;
import org.apache.log4j.Logger;

/**
 * DAO qui fait le lien entre l'objet Salarie ref et la table Salarie ref dans une BD MongoDB
 */
@DAO
public class SalarieRefDAO extends UtilisateurDAO<SalarieRef> {

    /**
     * Log les traitements de la DAO Salarie ref
     */
    private static Logger log = Logger.getLogger(SalarieRefDAO.class);

    /**
     * Instance unique de la DAO Salarie ref
     */
    private static SalarieRefDAO salarieRefDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un Salarie ref
     */
    private SalarieRefDAO() {
        super(SalarieRef.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Salarie ref
     */
    public static synchronized SalarieRefDAO getInstance() {
        if(salarieRefDAO == null)
            salarieRefDAO = new SalarieRefDAO();

        log.info("Get singleton SalarieRefDAO : " + salarieRefDAO);
        return salarieRefDAO;
    }
}

