package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Questionnaire;
import org.apache.log4j.Logger;

/**
 * DAO Questionnaire qui fait le lien entre l'objet Questionnaire et la table Questionnaire dans une BD MongoDB
 */
@DAO
public class QuestionnaireDAO extends ModelDAO<Questionnaire> {

    /**
     * Log les traitements de la DAO Questionnaire
     */
    private static Logger log = Logger.getLogger(QuestionnaireDAO.class);

    /**
     * Instance unique de la DAO Questionnaire
     */
    private static QuestionnaireDAO questionnaireDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un Questionnaire
     */
    private QuestionnaireDAO() {
        super(Questionnaire.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Questionnaire
     */
    public static synchronized QuestionnaireDAO getInstance() {
        if(questionnaireDAO == null)
            questionnaireDAO = new QuestionnaireDAO();

        log.info("Get singleton QuestionnaireDAO : " + questionnaireDAO);
        return questionnaireDAO;
    }
}