package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.OptionQuestion;
import org.apache.log4j.Logger;

/**
 * DAO OptionQuestion qui fait le lien entre l'objet OptionQuestion et la table OptionQuestion dans une BD MongoDB
 */
@DAO
public class OptionQuestionDAO extends ModelDAO<OptionQuestion> {

    /**
     * Log les traitements de la DAO OptionQuestion
     */
    private static Logger log = Logger.getLogger(OptionQuestionDAO.class);

    /**
     * Instance unique de la DAO OptionQuestion
     */
    private static OptionQuestionDAO questionDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'une OptionQuestion
     */
    private OptionQuestionDAO() {
        super(OptionQuestion.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO OptionQuestion
     */
    public static synchronized OptionQuestionDAO getInstance() {
        if(questionDAO == null)
            questionDAO = new OptionQuestionDAO();

        log.info("Get singleton OptionQuestionDAO : " + questionDAO);
        return questionDAO;
    }
}
