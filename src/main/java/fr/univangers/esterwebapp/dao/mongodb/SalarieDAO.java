package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Salarie;
import org.apache.log4j.Logger;

/**
 * DAO Salarie qui fait le lien entre l'objet Salarie et la table Salarie dans une BD MongoDB
 */
@DAO
public class SalarieDAO extends UtilisateurDAO<Salarie> {

    /**
     * Log les traitements de la DAO Salarie
     */
    private static Logger log = Logger.getLogger(SalarieDAO.class);

    /**
     * Instance unique de la DAO Salarie
     */
    private static SalarieDAO salarieDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un Salarie
     */
    private SalarieDAO() {
        super(Salarie.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO Salarie
     */
    public static synchronized SalarieDAO getInstance() {
        if(salarieDAO == null)
            salarieDAO = new SalarieDAO();

        log.info("Get singleton SalarieDAO : " + salarieDAO);
        return salarieDAO;
    }
}
