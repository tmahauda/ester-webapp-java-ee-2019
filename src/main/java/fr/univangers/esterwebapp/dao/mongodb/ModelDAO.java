package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.operation.OperationExport;
import fr.univangers.esterwebapp.operation.OperationModel;
import fr.univangers.esterwebapp.util.HibernateUtil;
import fr.univangers.esterwebapp.util.Util;
import fr.univangers.esterwebapp.util.jsontocsv.parser.JSONFlattener;
import fr.univangers.esterwebapp.util.jsontocsv.writer.CSVWriter;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import org.hibernate.Transaction;
import org.hibernate.ogm.OgmSession;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * DAO abstraite qui factorise les opérations CRUD d'une BD MongoDB qui sont communes à toutes les entités
 */
public abstract class ModelDAO<T extends Model> implements OperationModel<T>, OperationExport {

    /**
     * Log les traitements du Model DAO
     */
    private static Logger log = Logger.getLogger(ModelDAO.class);

    /**
     * Description du modele
     */
    protected Class<T> clazz;

    /**
     * Constructeur qui prend en paramétre la description d'un modele
     * @param clazz qui décrit le modele
     */
    public ModelDAO(Class<T> clazz) {
        this.clazz = clazz;
    }


    @Override
    public synchronized boolean create(T entity) {
        if(entity == null) return false;
        if(this.readById(entity.getUuid()) != null) return false;

        boolean create = false;

        Transaction transaction = null;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            session.persist(entity);
            transaction.commit();
            create = true;
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
            if(transaction != null) transaction.rollback();
        }

        return create;
    }

    public synchronized boolean merge(T entity) {
        if(entity == null) return false;
        if(this.readById(entity.getUuid()) != null) return false;

        boolean create = false;

        Transaction transaction = null;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            session.merge(entity);
            transaction.commit();
            create = true;
        }
        catch(Exception e) {
            if(transaction != null) transaction.rollback();
        }

        return create;
    }

    @Override
    public synchronized Collection<T> read() {
        Collection<T> collection = Collections.emptyList();

        Transaction transaction = null;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            String sql = String.format("SELECT t FROM %s t", this.clazz.getSimpleName());
            collection = session.createQuery(sql, this.clazz).getResultList();
            transaction.commit();
        }
        catch(Exception e) {
            if(transaction != null) transaction.rollback();
        }

        return collection;
    }

    @Override
    public synchronized boolean exportJSON(String pathFileJSON) {
        try {
            Files.write(Paths.get(pathFileJSON), this.exportJSON().getBytes());
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    @Override
    public synchronized boolean exportCSV(String pathFileCSV) {
        List<Map<String, String>> flatJson = JSONFlattener.parseJson(this.exportJSON());
        if(flatJson == null) return false;

        CSVWriter.writeToFile(CSVWriter.getCSV(flatJson, ";"), pathFileCSV);
        return true;
    }

    /**
     * Export les models en format JSON
     * @return un json
     */
    private synchronized String exportJSON() {
        Collection<T> models = this.read();
        if(models.isEmpty()) return "";

        StringBuilder json = new StringBuilder("");
        json.append("[\n");

        for(T model : models) {
            json.append(model.toJSON("\t"));
            json.append(",\n");
        }
        //On enlève la dernière virgule
        json.deleteCharAt(json.length()-2);
        json.append("\n");
        json.append("]");

        return json.toString();
    }

    @Override
    public synchronized T readLast() {
        return this.readOrder("DESC");
    }

    @Override
    public synchronized T readFirst() {
        return this.readOrder("ASC");
    }

    /**
     * Rechercher le premier modele enregistré dans la BD par trie ascendant sur la date de création ou
     * le dernier modele enregistré dans la BD par trie descendant sur la date de création
     * @param order le trie par ascendant ou descendant
     * @return
     */
    private synchronized T readOrder(String order) {
        if(Util.isNullOrEmpty(order)) return null;

        T entity = null;
        Transaction transaction = null;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            String sql = String.format("SELECT t FROM %s t ORDER BY %s %s", this.clazz.getSimpleName(), LangUtils.nameOfProperty(this.clazz, Model::getCreated), order);
            entity = session.createQuery(sql, this.clazz).setMaxResults(1).getSingleResult();
            transaction.commit();
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
            if(transaction != null) transaction.rollback();
        }

        return entity;
    }

    /**
     * Rechercher un modele par une propriété (mail, identifiant, etc.)
     * @param property le nom de la propriété (mail, identifiant, etc.)
     * @param value la valeur qui correpond
     * @return le modele trouvé qui correspond à la valeur évaluée sur la propriété
     */
    protected synchronized T read(String property, String value) {
        if(Util.isNullOrEmpty(property)) return null;
        if(Util.isNullOrEmpty(value)) return null;

        T entity = null;
        Transaction transaction = null;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            String sql = String.format("SELECT t FROM %s t WHERE %s = '%s'", this.clazz.getSimpleName(), property, value);
            entity = session.createQuery(sql, this.clazz).setMaxResults(1).getSingleResult();
            transaction.commit();
        }
        catch(Exception e) {
            if(transaction != null) transaction.rollback();
        }

        return entity;
    }

    @Override
    public synchronized T readById(UUID id) {
        if(id == null) return null;

        T entity = null;
        Transaction transaction = null;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            entity = session.get(this.clazz, id);
            transaction.commit();
        }
        catch(Exception e) {
            if(transaction != null) transaction.rollback();
        }

        return entity;
    }

    @Override
    public synchronized T readById(String id) {
        if(Util.isNullOrEmpty(id)) return null;

        String property = LangUtils.nameOfProperty(this.clazz, Model::getIdentifiant);
        log.info("Propriete " + property);
        log.info("Propriété à interroger : " + property);
        log.info("Valeur : " + id);
        return this.read(property, id);
    }

    @Override
    public synchronized boolean exportJSONById(String id, String pathFileJSON) {
        T model = this.readById(id);
        if(model == null) return false;

        try {
            Files.write(Paths.get(pathFileJSON), model.toJSON().getBytes());
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    @Override
    public synchronized boolean exportCSVById(String id, String pathFileCSV) {
        T model = this.readById(id);
        if(model == null) return false;

        List<Map<String, String>> flatJson = JSONFlattener.parseJson(model.toJSON());
        if(flatJson == null) return false;

        CSVWriter.writeToFile(CSVWriter.getCSV(flatJson, ";"), pathFileCSV);
        return true;
    }

    @Override
    public synchronized boolean update(T entity) {
        if(entity == null) return false;
        if(this.readById(entity.getUuid()) == null) return false;

        Transaction transaction = null;
        boolean update = false;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            session.merge(entity);
            transaction.commit();
            update = true;
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
            if(transaction != null) transaction.rollback();
        }

        return update;
    }

    @Override
    public synchronized boolean save(T entity) {
        if(this.create(entity)) return true;
        else return this.update(entity);
    }

    @Override
    public synchronized boolean delete(T entity) {
        if(entity == null) return false;
        if(this.readById(entity.getUuid()) == null) return false;

        Transaction transaction = null;
        boolean delete = false;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            session.remove(entity);
            transaction.commit();
            delete = true;
        }
        catch(Exception e) {
           // log.error(e.getMessage(), e);
            if(transaction != null) transaction.rollback();
        }

        return delete;
    }

    @Override
    public synchronized boolean delete() {
        if(this.read().isEmpty()) return false;

        Transaction transaction = null;
        boolean delete = false;

        try {
            OgmSession session = HibernateUtil.getSessionFactory().getCurrentSession();
            transaction = session.beginTransaction();
            String sql = String.format("db.%s.remove({})", this.clazz.getSimpleName());
            session.createNativeQuery(sql).executeUpdate();
            transaction.commit();
            delete = true;
        }
        catch(Exception e) {
            log.error(e.getMessage(), e);
            if(transaction != null) transaction.rollback();
        }

        return delete;
    }

    public synchronized T readByName(String nom) {
        if(Util.isNullOrEmpty(nom)) return null;

        String property = LangUtils.nameOfProperty(this.clazz, Model::getNom);
        log.info("Propriete " + property);
        log.info("Valeur : " + nom);
        return this.read(property, nom);
    }

}