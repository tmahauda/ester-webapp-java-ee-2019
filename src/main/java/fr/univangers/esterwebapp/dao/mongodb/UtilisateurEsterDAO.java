package fr.univangers.esterwebapp.dao.mongodb;

import fr.univangers.esterwebapp.model.UtilisateurEster;
import org.apache.log4j.Logger;

/**
 * DAO UtilisateurEster qui fait le lien entre l'objet UtilisateurEster et la table UtilisateurEster dans une BD MongoDB
 */
@DAO
public class UtilisateurEsterDAO extends ConnexionDAO<UtilisateurEster> {

    /**
     * Log les traitements de la DAO UtilisateurEster
     */
    private static Logger log = Logger.getLogger(UtilisateurEsterDAO.class);

    /**
     * Instance unique de la DAO UtilisateurEster
     */
    private static UtilisateurEsterDAO utilisateurEsterDAO = null;

    /**
     * Constructeur qui prend en paramétre la description d'un UtilisateurEster
     */
    private UtilisateurEsterDAO() {
        super(UtilisateurEster.class);
    }

    /**
     * Singleton qui assure l'unicité de la DAO
     * On veut eviter plusieurs DAO pour une entité
     * Une entité = Une DAO
     * @return l'instance de la DAO UtilisateurEster
     */
    public static synchronized UtilisateurEsterDAO getInstance() {
        if(utilisateurEsterDAO == null)
            utilisateurEsterDAO = new UtilisateurEsterDAO();

        log.info("Get singleton UtilisateurDAO : " + utilisateurEsterDAO);
        return utilisateurEsterDAO;
    }
}