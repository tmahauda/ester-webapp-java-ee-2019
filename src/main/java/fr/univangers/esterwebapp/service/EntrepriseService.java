package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.EntrepriseDAO;
import fr.univangers.esterwebapp.model.Entreprise;
import fr.univangers.esterwebapp.util.SecurityUtil;
import org.apache.log4j.Logger;

@Service
public class EntrepriseService extends ConnexionService<Entreprise> {

    /**
     * Log les traitements de l'entreprise service
     */
    private static Logger log = Logger.getLogger(EntrepriseService.class);

    private static EntrepriseService entrepriseService = null;

    private EntrepriseService() {
        super(EntrepriseDAO.getInstance());
    }

    public static synchronized EntrepriseService getInstance() {
        if(entrepriseService == null)
            entrepriseService = new EntrepriseService();

        log.info("Get singleton EntrepriseService : " + entrepriseService);
        return entrepriseService;
    }

    public synchronized Entreprise create(String id, String nom, boolean premiereConnexion, String motPasse, String email) {
        Entreprise entreprise = new Entreprise(id, nom, SecurityUtil.chiffrerMotPasse(motPasse), email, premiereConnexion);
        if(this.save(entreprise)) {
            return entreprise;
        }
        else {
            return null;
        }
    }
}