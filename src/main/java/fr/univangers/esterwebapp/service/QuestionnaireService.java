package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.QuestionnaireDAO;
import fr.univangers.esterwebapp.model.Question;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionEnum;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionnaireEnum;
import fr.univangers.esterwebapp.util.HibernateUtil;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import java.util.*;

@Service
public class QuestionnaireService extends ModelService<Questionnaire> {

    /**
     * Log les traitements du questionnaire service
     */
    private static Logger log = Logger.getLogger(QuestionnaireService.class);

    /**
     * Singleton qui contient une unique instance de questionnaire service
     */
    private static QuestionnaireService questionnaireService = null;

    /**
     * Constructeur qui recupere le singleton de la DAO questionnaire
     */
    private QuestionnaireService() {
        super(QuestionnaireDAO.getInstance());
    }

    /**
     * Retourne l'unique instance de questionnaire service
     * @return l'instance unique de questionnaire service
     */
    public static synchronized QuestionnaireService getInstance() {
        if(questionnaireService == null)
            questionnaireService = new QuestionnaireService();

        log.info("Get singleton QuestionnaireService : " + questionnaireService);
        return questionnaireService;
    }

    /**
     * Récupérer dans la BD ou créer un questionnaire à partir de la classe QuestionnaireEnum si non trouvé à partir du code
     * @param codeQuestionnaire
     * @return le questionnaire retrouvé dans la BD ou un nouveau questionnaire si code renseigné dans QuestionnaireEnum ou null
     */
    public synchronized Questionnaire getOrNewQuestionnaire(String codeQuestionnaire) {
        if(Util.isNullOrEmpty(codeQuestionnaire)) return null;

        //On vérifie si le questionnaire n'est pas déjà présent dans la BD
        Questionnaire questionnaire = this.readById(codeQuestionnaire);

        if(questionnaire != null) return questionnaire;

        //On vérifie si le questionnaire est renseigné dans la classe QuestionnaireEnum
        Optional<QuestionnaireEnum> optionalQuestionnaire = Arrays.stream(QuestionnaireEnum.values()).filter(q -> q.getCode().equals(codeQuestionnaire)).findFirst();
        if(!optionalQuestionnaire.isPresent()) return null;

        questionnaire = new Questionnaire(optionalQuestionnaire.get(), new Date(), new Date());
        this.save(questionnaire);

        return questionnaire;
    }

    public synchronized Questionnaire getOrNewQuestionnaireByName(String nom) {
        if(Util.isNullOrEmpty(nom)) return null;

        //On vérifie si le questionnaire n'est pas déjà présent dans la BD
        Questionnaire questionnaire = this.readByName(nom);
        log.info("QuestionnaireService : "+questionnaire);
        if(questionnaire != null) {
            return this.getOrNewQuestionnaire(questionnaire.getIdentifiant());
        }
        else return null;
    }

    /**
     * Récupérer dans la BD ou créer un questionnaire Lombalgie
     * @return le questionnaire Lombalgie
     */
    public synchronized Questionnaire getOrNewQuestionnaireLombalgie() {
        //On vérifie si le questionnaire n'est pas déjà crée
        Questionnaire lombalgie = this.readById(QuestionnaireEnum.LOMBALGIE.getCode());
        if(lombalgie != null) return lombalgie;

        lombalgie = new Questionnaire(QuestionnaireEnum.LOMBALGIE, new Date(), new Date());
        lombalgie.getQuestions().add(new Question(QuestionEnum.ECHELLE_BORG, lombalgie));
        lombalgie.getQuestions().add(new Question(QuestionEnum.PENCHER, lombalgie));

        this.save(lombalgie);

        return lombalgie;
    }

    /**
     * Récupérer dans la BD ou créer un questionnaire Chronique
     * @return le questionnaire Chronique
     */
    public synchronized Questionnaire getOrNewQuestionnaireChronique() {
        //On vérifie si le questionnaire n'est pas déjà crée
        Questionnaire chronique = this.readById(QuestionnaireEnum.CHRONIQUE.getCode());
        if(chronique != null) return chronique;

        chronique = new Questionnaire(QuestionnaireEnum.CHRONIQUE, new Date(), new Date());
        chronique.getQuestions().add(new Question(QuestionEnum.ECHELLE_BORG, chronique));
        chronique.getQuestions().add(new Question(QuestionEnum.REPETER_ACTIONS, chronique));
        chronique.getQuestions().add(new Question(QuestionEnum.TRAVAILLER_BRAS_EN_LAIR, chronique));
        chronique.getQuestions().add(new Question(QuestionEnum.FLECHIR_COUDE, chronique));
        chronique.getQuestions().add(new Question(QuestionEnum.PRESSER_OBJETS, chronique));
        chronique.getQuestions().add(new Question(QuestionEnum.INFLUENCER_DEROULEMENT_TRAVAIL, chronique));
        chronique.getQuestions().add(new Question(QuestionEnum.COLLEGUES_AIDENT_TACHES, chronique));

        this.save(chronique);

        return chronique;
    }

    /**
     * Importer toutes les données de références dans la BD pour un questionnaire
     * @param records contenant toutes les données
     * @param questionnaire le questionnaire auquel il faut rattacher les données
     * @param importeur l'utilisateur qui effectue l'import
     * @return vrai si tout importé. Faux dans le cas contraire
     */
    public synchronized boolean importDataFromFile(List<List<String>> records, Questionnaire questionnaire, UtilisateurEster importeur) {
        if(records == null) return false;
        if(records.isEmpty()) return false;
        if(questionnaire == null) return false;
        if(importeur == null) return false;

        //On désactive les logs durant l'import pour aller plus rapide
        HibernateUtil.activateLog(false);

        //On récupère le contenu du fichier
        log.info("Records : " + records);
        log.info("Nombre de réponses : " + (records.size() - 1));

        //Entetes = Questions + salariés
        List<String> codeEntetes = records.get(0);
        log.info("Codes entetes : " + codeEntetes);
        log.info("Nombre d'entetes: " + codeEntetes.size());

        //On crée un thread par question pour importer tous les résultats de la question
        //Traitement plus rapide
        Thread[] threadsQuestion = new Thread[codeEntetes.size()];

        //Parcours de toutes les questions
        for(int i=0; i<codeEntetes.size(); i++) {
            //On récupère d'abord le code de la question
            //Si c'est null on passe à la suivante
            String codeQuestion = codeEntetes.get(i).trim();
            log.info("Code Question : " + codeQuestion);
            if(Util.isNullOrEmpty(codeQuestion)) continue;

            //Puis on crée ou on récupere la question à partir du code dans la BD si trouvé
            Question question = QuestionService.getInstance().getOrNewQuestion(codeQuestion, questionnaire);
            log.info("Question : " + question);
            if(question == null) continue;

            //On importe toutes les réponses de la question courante géré par un thread
            ImportQuestionThread importQuestionThread = new ImportQuestionThread(records, codeEntetes, importeur, questionnaire, question, i);
            threadsQuestion[i] = new Thread(importQuestionThread);
            threadsQuestion[i].start();
        }

        //On attend que tous les thread aient terminé d'importer les réponses
        for(int i=0; i<threadsQuestion.length; i++) {
            if(threadsQuestion[i] != null) {
                try {
                    threadsQuestion[i].join();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    return false;
                }
            }
        }

        //On met à jour le questionnaire à la fin
        boolean save =  QuestionnaireService.getInstance().save(questionnaire);

        //A la fin on réactive les logs
        HibernateUtil.activateLog(true);

        return save;
    }
}