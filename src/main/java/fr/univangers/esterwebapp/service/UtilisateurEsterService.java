package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.UtilisateurEsterDAO;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import org.apache.log4j.Logger;

@Service
public class UtilisateurEsterService extends ConnexionService<UtilisateurEster> {

    /**
     * Log les traitements de l'utilisateur service
     */
    private static Logger log = Logger.getLogger(UtilisateurEsterService.class);

    private static UtilisateurEsterService utilisateurEsterService = null;

    private UtilisateurEsterService() {
        super(UtilisateurEsterDAO.getInstance());
    }

    public static synchronized UtilisateurEsterService getInstance() {
        if(utilisateurEsterService == null)
            utilisateurEsterService = new UtilisateurEsterService();

        log.info("Get singleton UtilisateurEsterService : " + utilisateurEsterService);
        return utilisateurEsterService;
    }

    public synchronized UtilisateurEster create(String identifiant, String nom, String email, String motPasse, Role role, boolean premiereConnexion) {
        UtilisateurEster utilisateurEster = new UtilisateurEster(identifiant,nom, email, motPasse, role, premiereConnexion);
        if(this.save(utilisateurEster)) {
            return utilisateurEster;
        }
        else {
            return null;
        }
    }
}