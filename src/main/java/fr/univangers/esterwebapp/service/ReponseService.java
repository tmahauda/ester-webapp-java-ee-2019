package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.ReponseDAO;
import fr.univangers.esterwebapp.model.Reponse;
import org.apache.log4j.Logger;

@Service
public class ReponseService extends ModelService<Reponse> {

    /**
     * Log les traitements du réponse service
     */
    private static Logger log = Logger.getLogger(ReponseService.class);

    private static ReponseService reponseService = null;

    private ReponseService() {
        super(ReponseDAO.getInstance());
    }

    public static synchronized ReponseService getInstance() {
        if(reponseService == null)
            reponseService = new ReponseService();

        log.info("Get singleton ReponseService : " + reponseService);
        return reponseService;
    }
}