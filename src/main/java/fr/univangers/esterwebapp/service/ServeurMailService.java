package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.dao.mongodb.ServeurMailDAO;
import fr.univangers.esterwebapp.model.ServeurMail;
import org.apache.log4j.Logger;

@Service
public class ServeurMailService extends ModelService<ServeurMail> {

    /**
     * Log les traitements du mail service
     */
    private static Logger log = Logger.getLogger(ServeurMailService.class);

    private static ServeurMailService serveurMailService = null;

    private static final String MAIL = "ester.chu.angers@gmail.com";
    private static final String ALIAS = "no-reply@ester.com";
    private static final String MOTPASSE = "Administrateur";
    private static final String HOST = "smtp.gmail.com";
    private static final int PORT = 587;

    private ServeurMailService() {
        super(ServeurMailDAO.getInstance());
    }

    public static synchronized ServeurMailService getInstance() {
        if(serveurMailService == null)
            serveurMailService = new ServeurMailService();

        log.info("Get singleton serveurMailService : " + serveurMailService);
        return serveurMailService;
    }

    public synchronized ServeurMail getOrNewServeurMailESTER() {
        //On vérifie si le serveur n'est pas déjà crée
        //On sait qu'il y a un unique serveur
        ServeurMail serveurMail = this.readFirst();
        if(serveurMail != null) return serveurMail;

        serveurMail = new ServeurMail(MAIL, ALIAS, MOTPASSE, HOST, PORT);

        this.save(serveurMail);

        return serveurMail;
    }
}