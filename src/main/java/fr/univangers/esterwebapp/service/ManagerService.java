package fr.univangers.esterwebapp.service;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.util.scanner.Manager;
import fr.univangers.esterwebapp.model.Connexion;
import fr.univangers.esterwebapp.model.Utilisateur;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;

public class ManagerService extends Manager<ModelService<? extends Model>> {

    /**
     * Log les traitements du manager service
     */
    private static Logger log = Logger.getLogger(ManagerService.class);

    private static ManagerService managerService = null;

    private ManagerService() {
        super(Service.class, ModelService.class);
    }

    public static synchronized ManagerService getInstance() {
        if(managerService == null)
            managerService = new ManagerService();

        log.info("Get singleton ManagerService : " + managerService);
        return managerService;
    }

    @SuppressWarnings("unchecked")
    public synchronized List<ConnexionService<Connexion>> getUtilisateursConnexionService() {

        List<ConnexionService<Connexion>> utilisateursConnexionService = new ArrayList<>();

        for(ModelService<? extends Model> modelService : this.entities) {
            log.info("Service : " + modelService);
            if(modelService instanceof ConnexionService) {
                log.info("Service utilisateur connexion: " + modelService + " ajouté");
                utilisateursConnexionService.add((ConnexionService<Connexion>) modelService);
            }
        }

        return utilisateursConnexionService;
    }

    public synchronized boolean updateUtilisateur(Utilisateur utilisateur) {
        boolean update = false;
        for(UtilisateurService<Utilisateur> utilisateurService : this.getUtilisateursService()) {
            update = utilisateurService.update(utilisateur);
            log.info("Update : " + update);
            if(update) return true;
        }
        return update;
    }

    @SuppressWarnings("unchecked")
    public synchronized List<UtilisateurService<Utilisateur>> getUtilisateursService() {

        List<UtilisateurService<Utilisateur>> utilisateursService = new ArrayList<>();

        for(ModelService<? extends Model> modelService : this.entities) {
            log.info("Service : " + modelService);
            if(modelService instanceof UtilisateurService) {
                log.info("Service utilisateur connexion: " + modelService + " ajouté");
                utilisateursService.add((UtilisateurService<Utilisateur>) modelService);
            }
        }

        return utilisateursService;
    }

    public synchronized Utilisateur getUtilisateurByIdentifiant(String identifiant) {
        for(UtilisateurService<Utilisateur> utilisateurService : this.getUtilisateursService()) {
            Utilisateur user = utilisateurService.readById(identifiant);
            log.info("User : " + user);
            if(user != null) return user;
        }

        return null;
    }
}