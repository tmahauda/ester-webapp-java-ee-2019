package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.Objects;

/**
 * Modele qui représente une Réponse référence à une question d'un questionnaire
 */
@Entity
public class ReponseRef extends Model {

    /**
     * Log les traitements d'une Reponse référence
     */
    private static Logger log = Logger.getLogger(ReponseRef.class);

    /**
     * Les valeurs répondus à la question
     */
    @Column(name = "valeurReponse")
    private String valeurReponse;

    /**
     * Le score de la réponse
     */
    @Column(name = "scoreReponse")
    private int scoreReponse;

    /**
     * Le salarié référence qui a répondu
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private SalarieRef repondant;

    /**
     * La question qui a été répondu
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private Question question;

    /**
     * Le résultat référence de la réponse
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private ResultatRef resultat;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public ReponseRef() {
        this("", "", 0);
    }

    /**
     * Constructeur avec une proposition
     * @param proposition
     */
    public ReponseRef(Proposition proposition) {
        this(proposition.getCode(), proposition.getNom(), proposition.getScore());
    }

    /**
     * Constructeur avec une proposition rattaché au salarié référence, resultat référence et question
     * @param proposition
     * @param repondant
     * @param resultat
     * @param question
     */
    public ReponseRef(Proposition proposition, SalarieRef repondant, ResultatRef resultat, Question question) {
        this(proposition.getCode(), proposition.getNom(), proposition.getScore(), repondant, resultat, question);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs sans salarié, resultat et question rattaché
     * @param identifiant
     * @param valeursReponse
     * @param scoreReponse
     */
    public ReponseRef(String identifiant, String valeurReponse, int scoreReponse) {
        this(identifiant, valeurReponse, scoreReponse, null, null, null);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs avec salarié référence, resultat référence et question rattaché
     * @param identifiant
     * @param valeursReponse
     * @param scoreReponse
     * @param repondant
     * @param resultat
     * @param question
     */
    public ReponseRef(String identifiant, String valeurReponse, int scoreReponse, SalarieRef repondant, ResultatRef resultat, Question question) {
        super(identifiant);
        this.valeurReponse = valeurReponse;
        log.info("Valeur réponse : " + this.valeurReponse);
        this.scoreReponse = scoreReponse;
        log.info("Score de la réponse : " + this.scoreReponse);
        this.repondant = repondant;
        log.info("Répondant : " + this.repondant);
        this.resultat = resultat;
        log.info("Résultat : " + this.resultat);
        this.question = question;
        log.info("Question : " + this.question);

        if(this.resultat != null && !this.resultat.getReponseParQuestion().contains(this)) {
            this.resultat.getReponseParQuestion().add(this);
            log.info("Ajout de la réponse dans la liste du résultat : " + this.resultat.getReponseParQuestion());
        }
    }

    /**
     * Getter valeursReponse
     *
     * @return valeursReponse
     */
    public String getValeurReponse() {
        return valeurReponse;
    }

    /**
     * Setter valeursReponse
     *
     * @param valeursReponse to set
     */
    public void setValeursReponse(String valeurReponse) {
        this.valeurReponse = valeurReponse;
    }

    /**
     * Getter scoreReponse
     *
     * @return scoreReponse
     */
    public int getScoreReponse() {
        return scoreReponse;
    }

    /**
     * Setter scoreReponse
     *
     * @param scoreReponse to set
     */
    public void setScoreReponse(int scoreReponse) {
        this.scoreReponse = scoreReponse;
    }

    /**
     * Getter repondant
     *
     * @return repondant
     */
    public SalarieRef getRepondant() {
        return repondant;
    }

    /**
     * Setter repondant
     *
     * @param repondant to set
     */
    public void setRepondant(SalarieRef repondant) {
        this.repondant = repondant;
    }

    /**
     * Getter question
     *
     * @return question
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Setter question
     *
     * @param question to set
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    /**
     * Getter resultat
     *
     * @return resultat
     */
    public ResultatRef getResultat() {
        return resultat;
    }

    /**
     * Setter resultat
     *
     * @param resultat to set
     */
    public void setResultat(ResultatRef resultat) {
        this.resultat = resultat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ReponseRef reponse = (ReponseRef) o;
        return scoreReponse == reponse.scoreReponse &&
                Objects.equals(valeurReponse, reponse.valeurReponse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), valeurReponse, scoreReponse);
    }

    @Override
    public String toString() {
        return "ReponseRef{" +
                "identifiant='" + identifiant + '\'' +
                ", valeurReponse=" + valeurReponse +
                ", scoreReponse=" + scoreReponse +
                ", uuid=" + uuid +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }
}