package fr.univangers.esterwebapp.model.modelenum.utilisateur;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import fr.univangers.esterwebapp.util.Util;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Age implements Proposition {

    ANS_15_19("15-19", "15-19", 0, 15, 19),
    ANS_20_24("20-24", "20-24", 0, 20, 24),
    ANS_25_29("25-29", "25-29", 0, 25, 29),
    ANS_30_34("30-34", "30-34", 0, 30, 34),
    ANS_35_39("35-39", "35-39", 0, 35, 39),
    ANS_40_44("40-44", "40-44", 0, 40, 44),
    ANS_45_49("45-49", "45-49", 0, 45, 49),
    ANS_50_54("50-54", "50-54", 0, 50, 54),
    ANS_55_59("55-59", "55-59", 0, 55, 59),
    ANS_60_64("60-64", "60-64", 0, 60, 64),
    ANS_65_69("65-69", "65-69", 0, 65, 69),
    ANS_70_74("70-74", "70-74", 0, 70, 74),
    ANS_75_79("75-79", "75-79", 0, 75, 79);

    private String code;
    private String nom;
    private Integer score;
    private int minAge;
    private int maxAge;

    Age() {
        this("", "", 0, 0, 0);
    }

    Age(String code, String nom, Integer score, int minAge, int maxAge) {
        this.code = code;
        this.nom = nom;
        this.score = score;
        this.minAge = minAge;
        this.maxAge = maxAge;
    }

    public static Age getProposition(String code) {
        Age a = SearchEnum.getProposition(values(), code);
        if(a != null) return a;
        if(Util.isNullOrEmpty(code)) return null;
        if(Util.isNullOrEmpty(code.trim())) return null;

        try {
            int age = Integer.parseInt(code.trim());
            return getAge(age);
        }catch (Exception e) {
            return null;
        }
    }

    public static Age getAge(int age) {
        for(Age a : values()) {
            if(age >= a.minAge && age <= a.maxAge) {
                return a;
            }
        }
        return null;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    /**
     * Getter minAge
     *
     * @return minAge
     */
    public int getMinAge() {
        return minAge;
    }

    /**
     * Getter maxAge
     *
     * @return maxAge
     */
    public int getMaxAge() {
        return maxAge;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Age[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Age[]>() {}, Age.class);
    }

    public static Age[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Age[]>() {}, Age.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Age{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}