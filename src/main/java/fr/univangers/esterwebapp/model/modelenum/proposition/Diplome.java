package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Diplome implements Proposition {

    PAS_DE_DIPLOME("1", "Pas de diplôme", 0),
    DFEO("2", "DFEO", 0),
    BEPC("3", "BEPC", 0),
    CAP_BEP("4","CAP, BEP", 0),
    BAC("5","BAC",0),
    BAC_PLUS_2("6","BAC+2",0),
    BAC_PLUS_2_SUP("7","> BAC+2",0);

    private String code;
    private String nom;
    private Integer score;

    Diplome() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Diplome(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Diplome getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Diplome[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Diplome[]>() {}, Diplome.class);
    }

    public static Diplome[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Diplome[]>() {}, Diplome.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Diplome{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
