package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum ContratTravail implements Proposition {

    CDI("1", "CDI", 0),
    CDD("2", "CDD", 0),
    INTERIM("4", "Intérim", 0),
    FONCTIONNAIRE("5","Fonctionnaire", 0),
    APPRENTI("6","Apprenti", 0),
    MESURE_EMPLOI("6" ,"Mesure pour l'emploi", 0);

    private String code;
    private String nom;
    private Integer score;

    ContratTravail() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    ContratTravail(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static ContratTravail getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static ContratTravail[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<ContratTravail[]>() {}, ContratTravail.class);
    }

    public static ContratTravail[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<ContratTravail[]>() {}, ContratTravail.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Contrat{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}
