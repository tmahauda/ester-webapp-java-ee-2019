package fr.univangers.esterwebapp.model.modelenum.utilisateur;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.model.Travail;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import fr.univangers.esterwebapp.model.modelenum.question.QuestionType;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import fr.univangers.esterwebapp.model.modelenum.proposition.*;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import java.lang.reflect.Method;
import java.util.Arrays;

public enum SalarieEnum implements Proposition {

    SEXE("AQ2", "Sexe", QuestionType.RADIO, 0, Sexe.values(), LangUtils.nameOfMethod(Salarie.class, Salarie::getSexe), null),
    AGE("AGENUM", "Age", QuestionType.RADIO, 0, Age.values(),  LangUtils.nameOfMethod(Salarie.class, Salarie::getAge), null),
    DEPARTEMENT_ACTIVITE("AQ33", "Département d'activité",QuestionType.RADIO, 0, Departement.values(), LangUtils.nameOfMethod(Salarie.class, Salarie::getTravail), LangUtils.nameOfMethod(Travail.class, Travail::getDepartementActivite)),
    REGION_ACTIVITE("AQ32", "Région d'activité",QuestionType.RADIO, 0, Region.values(), LangUtils.nameOfMethod(Salarie.class, Salarie::getTravail), LangUtils.nameOfMethod(Travail.class, Travail::getRegionActivite)),
    CODE_NAF("R3naf4", "Code NAF", QuestionType.RADIO, 0, NAF.values(), LangUtils.nameOfMethod(Salarie.class, Salarie::getTravail), LangUtils.nameOfMethod(Travail.class, Travail::getCodeNAF)),
    CODE_PCS("PCS03", "Code PCS nomenclature 2003", QuestionType.RADIO, 0, PCS.values(), LangUtils.nameOfMethod(Salarie.class, Salarie::getTravail), LangUtils.nameOfMethod(Travail.class, Travail::getCodePCS)),
    IDENTIFIANT("ID", "Identifiant", QuestionType.SHORTANSWER, 0, new Proposition[0], LangUtils.nameOfMethod(Salarie.class, Salarie::getIdentifiant),null);

    private static Logger log = Logger.getLogger(SalarieEnum.class);

    private String code;
    private String nom;
    private QuestionType questionType;
    private Integer score;
    private Proposition[] propositions;
    private String propertySalarie;
    private String propertyTravail;

    SalarieEnum() {
        this("", "", null, 0, new Proposition[0], "", "");
    }

    SalarieEnum(String code, String nom, QuestionType questionType, Integer score, Proposition[] propositions, String propertySalarie, String propertyTravail) {
        this.code = code;
        this.nom = nom;
        this.questionType = questionType;
        this.score = score;
        this.propositions = propositions;
        this.propertySalarie = propertySalarie;
        this.propertyTravail = propertyTravail;
    }

    public String getValue(Salarie salarie) {
        log.error("Salarié : " + salarie);
        log.error("Property salarié : " + this.propertySalarie);
        log.error("Property travail : " + this.propertyTravail);

        Object valueSalarie = this.getObjectValue(salarie, this.propertySalarie);
        if(valueSalarie == null) return Manquant.DONNEE_MANQUANTE.getCode();
        if(this.propertyTravail == null) {
            if(valueSalarie instanceof Model) return ((Model)valueSalarie).getIdentifiant();
            else if(valueSalarie instanceof Proposition) return ((Proposition)valueSalarie).getCode();
            else {
                String strValue = valueSalarie.toString();
                if(Util.isNullOrEmpty(strValue)) return Manquant.DONNEE_MANQUANTE.getCode();
                else return strValue;
            }
        } else {
            Object valueTravail = this.getObjectValue(valueSalarie, this.propertyTravail);
            if(valueTravail == null) return Manquant.DONNEE_MANQUANTE.getCode();
            else if(valueTravail instanceof Model) return ((Model)valueTravail).getIdentifiant();
            else if(valueTravail instanceof Proposition) return ((Proposition)valueTravail).getCode();
            else {
                String strValue = valueTravail.toString();
                if(Util.isNullOrEmpty(strValue)) return Manquant.DONNEE_MANQUANTE.getCode();
                else return strValue;
            }
        }
    }

    private <T> Object getObjectValue(T object, String property) {
        Class<?> clazz = object.getClass();
        try {
            Method method = clazz.getMethod(property);
            return method.invoke(object);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    public static SalarieEnum getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public Proposition[] getPropositions() {
        return propositions;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static SalarieEnum[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<SalarieEnum[]>() {}, SalarieEnum.class);
    }

    public static SalarieEnum[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<SalarieEnum[]>() {}, SalarieEnum.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "SalarieEnum{" +
                "code='" + code + '\'' +
                ", nom='" + nom + '\'' +
                ", questionType=" + questionType +
                ", score=" + score +
                ", propositions=" + Arrays.toString(propositions) +
                '}';
    }

}
