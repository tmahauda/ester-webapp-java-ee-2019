package fr.univangers.esterwebapp.model.modelenum.utilisateur;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import org.json.JSONArray;
import org.json.JSONObject;

public enum Sexe implements Proposition {

    HOMME("0", "Homme", 0),
    FEMME("1", "Femme", 0);

    public static String CODE_SEXE = "AQ2";

    private String code;
    private String nom;
    private Integer score;

   Sexe() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    Sexe(String code, String nom, Integer score) {
        this.code = code;
        this.nom = nom;
        this.score = score;
    }

    public static Sexe getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static Sexe[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<Sexe[]>() {}, Sexe.class);
    }

    public static Sexe[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<Sexe[]>() {}, Sexe.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "Sexe{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}