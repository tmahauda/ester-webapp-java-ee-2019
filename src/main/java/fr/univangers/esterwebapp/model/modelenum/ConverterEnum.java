package fr.univangers.esterwebapp.model.modelenum;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import fr.univangers.esterwebapp.model.modelenum.proposition.Proposition;
import fr.univangers.esterwebapp.util.Util;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.File;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;

public class ConverterEnum {

    private static Logger log = Logger.getLogger(ConverterEnum.class);

    public static String FILE_PATH = "src/main/webapp/public";
    private static final String FILE_JSON = "json";
    private static final String FILE_CSV = "csv";

    private ConverterEnum() {}

    public static boolean writeJSONArray(Proposition[] propositions) {
        String fileName = propositions.getClass().getSimpleName().replaceAll("\\[]", "");
        log.info("File name : " + fileName);
        String filePath = String.format("%s/%s/proposition/%s.%s", FILE_PATH, FILE_JSON, fileName, FILE_JSON);
        log.info("File path : " + filePath);

        try {
            JSONArray propositionsArray = toJSONArray(propositions);
            String json = propositionsArray.toString(1);
            File file = new File(filePath);
            FileUtils.writeStringToFile(file, json, StandardCharsets.UTF_8, false);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean writeJSONObject(Proposition proposition) {
        String fileName = proposition.getClass().getSimpleName().replaceAll("\\[]", "");
        String filePath = String.format("%s/%s/proposition/%s.%s", FILE_PATH, FILE_JSON, fileName, FILE_JSON);

        try {
            JSONObject propositionObject = toJSONObject(proposition);
            String json = propositionObject.toString(1);
            File file = new File(filePath);
            FileUtils.writeStringToFile(file, json, StandardCharsets.UTF_8, false);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean writeCSVObject(Proposition proposition) {
        String fileName = proposition.getClass().getSimpleName().replaceAll("\\[]", "");
        String filePath = String.format("%s/%s/proposition/%s.%s", FILE_CSV, FILE_JSON, fileName, FILE_CSV);

        try {
            String csv = toCSVObject(proposition);
            File file = new File(filePath);
            FileUtils.writeStringToFile(file, csv, StandardCharsets.UTF_8, false);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    public static boolean writeCSVArray(Proposition[] propositions) {
        String fileName = propositions.getClass().getSimpleName().replaceAll("\\[]", "");
        String filePath = String.format("%s/%s/proposition/%s.%s", FILE_PATH, FILE_CSV, fileName, FILE_CSV);

        try {
            String csv = toCSVArray(propositions);
            File file = new File(filePath);
            FileUtils.writeStringToFile(file, csv, StandardCharsets.UTF_8, false);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Proposition> T[] readJSONArray(TypeReference<T[]> type, Class<T> clazz) {
        String fileName = clazz.getSimpleName().replaceAll("\\[]", "");
        log.info("File name : " + fileName);
        String filePath = String.format("%s/%s/proposition/%s.%s", FILE_PATH, FILE_JSON, fileName, FILE_JSON);
        log.info("File path : " + filePath);

        try {
            String json = FileUtils.readFileToString(new File(filePath), StandardCharsets.UTF_8);
            log.info("JSON content : " + json);

            if(Util.isNullOrEmpty(json)) return (T[]) Array.newInstance(clazz, 0);

            ObjectMapper objectMapper = new ObjectMapper();
            T[] propositions = objectMapper.readValue(json, type);
            log.info("Propositions : " + propositions);

            return propositions;

        } catch(Exception e) {
            log.error(e.getMessage(), e);
            return (T[]) Array.newInstance(clazz, 0);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Proposition> T[] readCSVArray(TypeReference<T[]> type, Class<T> clazz) {
        return (T[]) Array.newInstance(clazz, 0);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Proposition> T readJSONObject(TypeReference<T> type, Class<T> clazz) {
        String fileName = clazz.getSimpleName().replaceAll("\\[]", "");
        log.info("File name : " + fileName);
        String filePath = String.format("%s/%s/proposition/%s.%s", FILE_PATH, FILE_JSON, fileName, FILE_JSON);
        log.info("File path : " + filePath);

        try {
            String json = FileUtils.readFileToString(new File(filePath), StandardCharsets.UTF_8);
            log.info("JSON content : " + json);

            if(Util.isNullOrEmpty(json)) return null;

            ObjectMapper objectMapper = new ObjectMapper();
            T proposition = objectMapper.readValue(json, type);
            log.info("Propositions : " + proposition);

            return proposition;

        } catch(Exception e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public static JSONArray toJSONArray(Proposition[] propositions) {
        JSONArray propositionsArray = new JSONArray();

        for(Proposition proposition : propositions) {
            JSONObject propositionObject = toJSONObject(proposition);
            propositionsArray.put(propositionObject);
        }

        return propositionsArray;
    }

    public static String toCSVArray(Proposition[] propositions) {
        JSONArray jsonArray = toJSONArray(propositions);
        return CDL.toString(jsonArray).replace(',', ';');
    }

    @SuppressWarnings("unchecked")
    public static JSONObject toJSONObject(Proposition proposition) {
        JSONObject propositionObject = new JSONObject();

        propositionObject.put(LangUtils.nameOfProperty(Proposition.class, Proposition::getCode), proposition.getCode().toUpperCase());
        propositionObject.put(LangUtils.nameOfProperty(Proposition.class, Proposition::getNom), proposition.getNom());
        propositionObject.put(LangUtils.nameOfProperty(Proposition.class, Proposition::getScore), proposition.getScore());

        return propositionObject;
    }

    public static String toCSVObject(Proposition proposition) {
        JSONObject propositionObject = toJSONObject(proposition);
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(propositionObject);
        return CDL.toString(jsonArray).replace(',', ';');
    }
}
