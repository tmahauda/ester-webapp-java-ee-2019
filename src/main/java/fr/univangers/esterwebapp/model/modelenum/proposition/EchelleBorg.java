package fr.univangers.esterwebapp.model.modelenum.proposition;

import com.fasterxml.jackson.core.type.TypeReference;
import fr.univangers.esterwebapp.model.modelenum.ConverterEnum;
import fr.univangers.esterwebapp.model.modelenum.SearchEnum;
import org.json.JSONArray;
import org.json.JSONObject;

public enum EchelleBorg implements Proposition {

    PAS_EFFORT("6", "Pas d'effort du tout", 0),
    EXTREMEMENT_LEGER_1("7", "Extrêmement léger", 0),
    EXTREMEMENT_LEGER_2("8", "", 0),
    TRES_LEGER_1("9", "Très léger", 0),
    TRES_LEGER_2("10", "", 0),
    LEGER_1("11", "Léger", 0),
    LEGER_2("12", "", 0),
    PEU_DUR_1("13", "Un peu dur", 0),
    PEU_DUR_2("14", "", 0),
    DUR_1("15", "Dur", 2),
    DUR_2("16", "", 2),
    TRES_DUR_1("17", "Très Dur", 2),
    TRES_DUR_2("18", "", 2),
    EXTREMEMENT_DUR("19", "Extrêmement dur", 2),
    EPUISSANT("20", "Epuissant", 2);

    private String code;
    private String nom;
    private Integer score;

    EchelleBorg() {
        this.code = "";
        this.nom = "";
        this.score = 0;
    }

    EchelleBorg(String code, String nom, Integer score) {
        this.code = code;
        this.nom = code + " " + nom;
        this.score = score;
    }

    public static EchelleBorg getProposition(String code) {
        return SearchEnum.getProposition(values(), code);
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getNom() {
        return this.nom;
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    //Converter JSON/CSV

    @Override
    public JSONObject toJSONObject() {
        return ConverterEnum.toJSONObject(this);
    }

    @Override
    public String toCSVObject() {
        return ConverterEnum.toCSVObject(this);
    }

    public static JSONArray toJSONArray() {
        return ConverterEnum.toJSONArray(values());
    }

    public static String toCSVArray() {
        return ConverterEnum.toCSVArray(values());
    }

    //Converter read JSON/CSV

    public static EchelleBorg[] readJSONArray() {
        return ConverterEnum.readJSONArray(new TypeReference<EchelleBorg[]>() {}, EchelleBorg.class);
    }

    public static EchelleBorg[] readCSVArray() {
        return ConverterEnum.readCSVArray(new TypeReference<EchelleBorg[]>() {}, EchelleBorg.class);
    }

    //Converter write JSON/CSV

    @Override
    public boolean writeJSONObject() {
        return ConverterEnum.writeJSONObject(this);
    }

    @Override
    public boolean writeCSVObject() {
        return ConverterEnum.writeCSVObject(this);
    }

    public static boolean writeJSONArray() {
        return ConverterEnum.writeJSONArray(values());
    }

    public static boolean writeCSVArray() {
        return ConverterEnum.writeCSVArray(values());
    }

    @Override
    public String toString() {
        return "EchelleBorg{" +
                "code=" + code +
                ", nom='" + nom + '\'' +
                ", score=" + score +
                '}';
    }
}