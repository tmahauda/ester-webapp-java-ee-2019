package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.model.modelenum.proposition.Departement;
import fr.univangers.esterwebapp.model.modelenum.proposition.NAF;
import fr.univangers.esterwebapp.model.modelenum.proposition.PCS;
import fr.univangers.esterwebapp.model.modelenum.proposition.Region;
import fr.univangers.esterwebapp.model.modelenum.proposition.converter.ConverterNAF;
import fr.univangers.esterwebapp.model.modelenum.proposition.converter.ConverterPCS;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.Objects;

@Embeddable
public class Travail {

    /**
     * Log les traitements d'un Travail
     */
    private static Logger log = Logger.getLogger(Travail.class);

    /**
     * Le departement d'activité du salarié
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "départementActivite")
    private Departement departementActivite;

    /**
     * La région d'activité du salarié
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "regionActivite")
    private Region regionActivite;

    /**
     * Le code NAF (Nomenclature d'activités française)
     */
    @Convert(converter = ConverterNAF.class)
    @Column(name = "codeNAF")
    private NAF codeNAF;

    /**
     * Le code PCS (Professions et catégories socioprofessionnelles)
     */
    @Convert(converter = ConverterPCS.class)
    @Column(name = "codePCS")
    private PCS codePCS;


    /**
     * Constructeur par défaut pour Hibernate
     */
    public Travail() { }

    /**
     * Constructeur avec des arguments pour initialiser les attributs
     * @param departementActivite
     * @param regionActivite
     * @param codeNAF
     * @param codePCS
     */
    public Travail(Departement departementActivite, Region regionActivite, NAF codeNAF, PCS codePCS) {
        this.departementActivite = departementActivite;
        log.info("Département activité : " + this.departementActivite);
        this.regionActivite = regionActivite;
        log.info("Région activité : " + this.regionActivite);

        this.codeNAF = codeNAF;
        log.info("Code NAF : " + this.codeNAF);

        this.codePCS = codePCS;
        log.info("Code PCS : " + codePCS);

    }

    /**
     * Getter departementActivite
     *
     * @return departementActivite
     */
    public Departement getDepartementActivite() {
        return departementActivite;
    }

    /**
     * Setter departementActivite
     *
     * @param departementActivite to set
     */
    public void setDepartementActivite(Departement departementActivite) {
        this.departementActivite = departementActivite;
    }

    /**
     * Getter regionActivite
     *
     * @return regionActivite
     */
    public Region getRegionActivite() {
        return regionActivite;
    }

    /**
     * Setter regionActivite
     *
     * @param regionActivite to set
     */
    public void setRegionActivite(Region regionActivite) {
        this.regionActivite = regionActivite;
    }

    /**
     * Getter codeNAF
     *
     * @return codeNAF
     */
    public NAF getCodeNAF() {
        return codeNAF;
    }

    /**
     * Setter codeNAF
     *
     * @param codeNAF to set
     */
    public void setCodeNAF(NAF codeNAF) {
        this.codeNAF = codeNAF;
    }

    /**
     * Getter codePCS
     *
     * @return codePCS
     */
    public PCS getCodePCS() {
        return codePCS;
    }

    /**
     * Setter codePCS
     *
     * @param codePCS to set
     */
    public void setCodePCS(PCS codePCS) {
        this.codePCS = codePCS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Travail travail = (Travail) o;
        return departementActivite == travail.departementActivite &&
                regionActivite == travail.regionActivite &&
                Objects.equals(codeNAF, travail.codeNAF) &&
                Objects.equals(codePCS, travail.codePCS) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(departementActivite, regionActivite, codeNAF, codePCS);
    }

    @Override
    public String toString() {
        return "Travail{" +
                "departementActivite=" + departementActivite +
                ", regionActivite=" + regionActivite +
                ", codeNAF=" + codeNAF +
                ", codePCS=" + codePCS +
                '}';
    }
}
