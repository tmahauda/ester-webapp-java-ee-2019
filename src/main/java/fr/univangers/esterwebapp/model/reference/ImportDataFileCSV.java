package fr.univangers.esterwebapp.model.reference;

import org.apache.log4j.Logger;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Classe qui permet d'importer les données d'un fichier CSV dans la BD
 * @param <T> le modele
 */
public class ImportDataFileCSV extends ImportDataFile {

    private static Logger log = Logger.getLogger(ImportDataFileCSV.class);

    public ImportDataFileCSV(Part file) {
        super(file);
    }

    @Override
    public boolean read() {
        try(InputStreamReader reader = new InputStreamReader(this.inputStream, StandardCharsets.UTF_8)) {
            BufferedReader csvReader = new BufferedReader(reader);
            String row = csvReader.readLine();
            while (row != null) {
                if(!row.contains(";")) return false;
                String[] data = row.split(";");
                if(data.length == 0) return false;
                records.add(Arrays.asList(data));
                row = csvReader.readLine();
            }
            csvReader.close();
            return true;
        }
        catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }
}
