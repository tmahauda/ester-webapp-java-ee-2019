package fr.univangers.esterwebapp.model.reference;

import org.apache.log4j.Logger;

import javax.servlet.http.Part;

/**
 * Classe qui permet d'importer les données d'un fichier JSON dans la BD
 * @param <T> le modele
 */
public class ImportDataFileJSON extends ImportDataFile {

    private static Logger log = Logger.getLogger(ImportDataFileJSON.class);

    public ImportDataFileJSON(Part file) {
        super(file);
    }

    @Override
    public boolean read() {
        return false;
    }
}
