package fr.univangers.esterwebapp.model.reference;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

/**
 * Classe qui permet d'exporter les données d'un model en format CSV
 * @param <T> le modele
 */
public class ExportDataFileCSV<T extends Model> extends ExportDataFile<T> {

    private static Logger log = Logger.getLogger(ExportDataFileCSV.class);

    public ExportDataFileCSV(T model) {
        super(model);
    }

    @Override
    public boolean write() {
        this.records = this.model.toCSV(";");
        log.info("Records CSV : " + this.records);
        return !Util.isNullOrEmpty(this.records);
    }
}
