package fr.univangers.esterwebapp.model.reference;

import fr.univangers.esterwebapp.model.Model;

/**
 * Classe qui permet d'exporter les données d'un model
 * @param <T> le modele
 */
public abstract class ExportDataFile<T extends Model> {

    /**
     * Le model dont on souhaite récupérer les données
     */
    protected T model;

    /**
     * Les données du modèle sous forme de chaine de caractere
     */
    protected String records;

    /**
     * Constructeur
     * @param model
     */
    public ExportDataFile(T model) {
        this.model = model;
    }

    /**
     * Méthode abstraite qui permet d'écrire le format souhaité (XML, JSON, CSV, etc.)
     * @return vrai si écriture effectué. Sinon faux dans le cas contraire
     */
    public abstract boolean write();

    /**
     * Getter model
     *
     * @return model
     */
    public T getModel() {
        return model;
    }

    /**
     * Setter model
     *
     * @param model to set
     */
    public void setModel(T model) {
        this.model = model;
    }

    /**
     * Getter records
     *
     * @return records
     */
    public String getRecords() {
        return records;
    }

    /**
     * Setter records
     *
     * @param records to set
     */
    public void setRecords(String records) {
        this.records = records;
    }
}
