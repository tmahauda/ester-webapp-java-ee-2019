package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.util.SecurityUtil;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.List;
import java.util.Objects;

/**
 * Model abstrait Connexion permettant d'authentifier un Utilisateur par email et mot de passe
 */
@MappedSuperclass
public abstract class Connexion extends Utilisateur {

    /**
     * Log des traitements de Connexion d'un utilisateur
     */
    private static Logger log = Logger.getLogger(Connexion.class);

    /**
     * L'adresse mail unique de l'utilisateur
     */
    @Column(name = "email", unique = true, nullable = false)
    protected String email;

    /**
     * Le mot de passe de l'utilisateur
     */
    @Column(name = "password", nullable = false)
    protected String password;

    /**
     * Constructeur avec des paramétres pour initialiser les attributs
     * @param identifiant qui identifie l'utilisateur
     * @param email l'adresse mail de l'utilisateur
     * @param password le mot de passe de l'utilisateur
     * @param role le rôle de l'utilisateur
     * @param isFirstConnexion permettant de savoir si c'est la première connexion ou non de l'utilisateur
     */
    public Connexion(String identifiant, String email, String password, Role role, boolean isFirstConnexion) {
        super(identifiant, role, isFirstConnexion);
        this.email = email;
        log.info("Email : " + this.email);
        this.password = SecurityUtil.chiffrerMotPasse(password);
        log.info("Password : " + password);
        log.info("Password chiffré : " + this.password);
    }

    /**
     * Getter email
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter email
     * @param email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter password
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter password
     * @param password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Connexion connexion = (Connexion) o;
        return Objects.equals(email, connexion.email) &&
                Objects.equals(password, connexion.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), email, password);
    }

    @Override
    public String toString() {
        return "Connexion{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", identifiant='" + identifiant + '\'' +
                ", role=" + role +
                ", firstConnexion=" + firstConnexion +
                ", created =" + created +
                ", updated =" + updated +
                ", uuid=" + uuid +
                '}';
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = super.toJSON(clazzFrom, indentation);

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Connexion::getEmail) + "\":\"" + this.getEmail() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Connexion::getPassword) + "\":\"" + this.getPassword() + "\",\n";

        return json;
    }
}