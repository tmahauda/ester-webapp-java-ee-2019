package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.*;

/**
 * Modele qui représente un résultat
 */
@Entity
public class Resultat extends Model {

    /**
     * Log les traitements d'un Résultat
     */
    private static Logger log = Logger.getLogger(Resultat.class);

    /**
     * La date de lien au salarié
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "dateLienAuSalarie")
    private Date dateLienAuSalarie;

    /**
     * La date de la réponse
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "dateReponse")
    private Date dateReponse;

    /**
     * Score global du résultat
     * Somme des scores de chaque reponse
     */
    @Column(name = "scoreGlobal")
    private int scoreGlobal;

    /**
     * Le questionnaire
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Questionnaire questionnaireRepondu;

    /**
     * L'ensemble des réponses du questionnaire
     */
    @OneToMany(mappedBy = "resultat", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Reponse> reponseParQuestion;

    /**
     * L'utilisateur qui gére ce résultat
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private UtilisateurEster personneAssociant;

    /**
     * Le salarié qui a répondu au résultat
     */
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private Salarie personneRepondant;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public Resultat() {
        this("", new Date(), new Date(), 0);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs sans questionnaire, utilisateur et salarié
     * @param identifiant
     * @param dateLienAuSalarie
     * @param dateReponse
     * @param scoreGlobal
     */
    public Resultat(String identifiant, Date dateLienAuSalarie, Date dateReponse, int scoreGlobal) {
        this(identifiant, dateLienAuSalarie, dateReponse, scoreGlobal, null, null, null);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs avec questionnaire, utilisateur et salarié
     * @param identifiant
     * @param dateLienAuSalarie
     * @param dateReponse
     * @param scoreGlobal
     * @param questionnaireRepondu
     * @param personneAssociant
     * @param personneRepondant
     */
    public Resultat(String identifiant, Date dateLienAuSalarie, Date dateReponse, int scoreGlobal, Questionnaire questionnaireRepondu,
                    UtilisateurEster personneAssociant, Salarie personneRepondant) {
        super(identifiant);
        this.dateLienAuSalarie = dateLienAuSalarie;
        log.info("Date lien au salarié : " + this.dateLienAuSalarie);
        this.dateReponse = dateReponse;
        log.info("Date de réponse : " + this.dateReponse);
        this.scoreGlobal = scoreGlobal;
        log.info("Score global : " + this.scoreGlobal);
        this.questionnaireRepondu = questionnaireRepondu;
        log.info("Questionnaire répondu : " + this.questionnaireRepondu);
        this.personneAssociant = personneAssociant;
        log.info("Personne associant : " + this.personneAssociant);
        this.personneRepondant = personneRepondant;
        log.info("Personne répondant : " + this.personneRepondant);
        this.reponseParQuestion = new ArrayList<>();

        if(this.questionnaireRepondu != null && !questionnaireRepondu.getResultats().contains(this)) {
            this.questionnaireRepondu.getResultats().add(this);
            log.info("Ajout du résultat dans le questionnaire : " + this.questionnaireRepondu.getResultats());
        }
    }

    /**
     * Getter dateLienAuSalarie
     *
     * @return dateLienAuSalarie
     */
    public Date getDateLienAuSalarie() {
        return dateLienAuSalarie;
    }

    /**
     * Setter dateLienAuSalarie
     *
     * @param dateLienAuSalarie to set
     */
    public void setDateLienAuSalarie(Date dateLienAuSalarie) {
        this.dateLienAuSalarie = dateLienAuSalarie;
    }

    /**
     * Getter dateReponse
     *
     * @return dateReponse
     */
    public Date getDateReponse() {
        return dateReponse;
    }

    /**
     * Setter dateReponse
     *
     * @param dateReponse to set
     */
    public void setDateReponse(Date dateReponse) {
        this.dateReponse = dateReponse;
    }

    /**
     * Getter scoreGlobal
     *
     * @return scoreGlobal
     */
    public int getScoreGlobal() {
        return scoreGlobal;
    }

    /**
     * Setter scoreGlobal
     *
     * @param scoreGlobal to set
     */
    public void setScoreGlobal(int scoreGlobal) {
        this.scoreGlobal = scoreGlobal;
    }

    /**
     * Getter questionnaireRepondu
     *
     * @return questionnaireRepondu
     */
    public Questionnaire getQuestionnaireRepondu() {
        return questionnaireRepondu;
    }

    /**
     * Setter questionnaireRepondu
     *
     * @param questionnaireRepondu to set
     */
    public void setQuestionnaireRepondu(Questionnaire questionnaireRepondu) {
        this.questionnaireRepondu = questionnaireRepondu;
    }

    /**
     * Getter reponseParQuestion
     *
     * @return reponseParQuestion
     */
    public Collection<Reponse> getReponseParQuestion() {
        return reponseParQuestion;
    }

    /**
     * Setter reponseParQuestion
     *
     * @param reponseParQuestion to set
     */
    public void setReponseParQuestion(Collection<Reponse> reponseParQuestion) {
        this.reponseParQuestion = reponseParQuestion;
    }

    /**
     * Getter personneAssociant
     *
     * @return personneAssociant
     */
    public UtilisateurEster getPersonneAssociant() {
        return personneAssociant;
    }

    /**
     * Setter personneAssociant
     *
     * @param personneAssociant to set
     */
    public void setPersonneAssociant(UtilisateurEster personneAssociant) {
        this.personneAssociant = personneAssociant;
    }

    /**
     * Getter personneRepondant
     *
     * @return personneRepondant
     */
    public Salarie getPersonneRepondant() {
        return personneRepondant;
    }

    /**
     * Setter personneRepondant
     *
     * @param personneRepondant to set
     */
    public void setPersonneRepondant(Salarie personneRepondant) {
        this.personneRepondant = personneRepondant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Resultat resultat = (Resultat) o;
        return scoreGlobal == resultat.scoreGlobal &&
                Objects.equals(dateLienAuSalarie, resultat.dateLienAuSalarie) &&
                Objects.equals(dateReponse, resultat.dateReponse);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dateLienAuSalarie, dateReponse, scoreGlobal);
    }

    @Override
    public String toString() {
        return "Resultat{" +
                "dateLienAuSalarie=" + dateLienAuSalarie +
                ", dateReponse=" + dateReponse +
                ", scoreGlobal=" + scoreGlobal +
                ", reponseParQuestion=" + reponseParQuestion +
                ", uuid=" + uuid +
                ", identifiant=" + identifiant +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = "";

        json += indentation + "{\n";

        json += super.toJSON(clazzFrom, indentation);

        json = this.toJSON(clazzFrom, indentation, json, Reponse.class, LangUtils.nameOfProperty(this.getClass(), Resultat::getReponseParQuestion), this.getReponseParQuestion());

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Resultat::getDateLienAuSalarie) + "\":\"" + this.getDateLienAuSalarie() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Resultat::getDateReponse) + "\":\"" + this.getDateReponse() + "\",\n";
        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Resultat::getScoreGlobal) + "\":" + this.getScoreGlobal() + "\n";

        json += indentation + "}";

        return json;
    }
}