package fr.univangers.esterwebapp.model;

import org.apache.log4j.Logger;
import javax.mail.*;
import javax.persistence.*;
import java.util.*;

/**
 * Modele qui représente un ServeurMail permettant de se connecter à la boite mail
 */
@Entity
public class ServeurMail extends Model {

    /**
     * Log les traitements d'un ServeurMail
     */
    private static Logger log = Logger.getLogger(ServeurMail.class);

    /**
     * Alias pour masquer la véritable adresse mail
     * Par exemple afficher no-reply@...
     */
    @Column(name = "alias")
    private String alias;

    /**
     * Mot de passe pour se connecter à la boite mail
     */
    @Column(name = "password")
    private String password;

    /**
     * Host utilisé (gmail, yahoo, ...)
     */
    @Column(name = "host")
    private String host;

    /**
     * Port utilisé par le host
     */
    @Column(name = "port")
    private int port;

    /**
     * Les mails que possèdent le serveur
     */
    @OneToMany(mappedBy = "serveurMail", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Collection<Mail> mails;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public ServeurMail() {
        this("", "", "", "", 0);
    }

    /**
     * Constructeur avec argument pour initialiser les attributs
     * @param email
     * @param alias
     * @param password
     * @param host
     * @param port
     */
    public ServeurMail(String email, String alias, String password, String host, int port) {
        super(email);
        this.nom="";
        this.alias = alias;
        log.info("Alias : " + this.alias);
        this.password = password;
        log.info("Password : " + this.password);
        this.host = host;
        log.info("Host : " + this.host);
        this.port = port;
        log.info("Port : " + this.port);
        this.mails = new ArrayList<>();
    }

    /**
     * Méthode qui permet de récupérer les paramétres de connexion à une boite mail
     * @return les paramétres de connexion
     */
    private Properties getProperties() {
        Properties prop = new Properties();
        prop.put("mail.transport.protocol", "smtp");
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", this.host);
        prop.put("mail.smtp.port", this.port);
        return prop;
    }

    /**
     * Méthode qui permet de récupérer la session de connexion d'une boite mail
     * @return la session
     */
    public Session getSession() {
        return Session.getDefaultInstance(this.getProperties(), new Authenticator()
        {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(identifiant, password);
            }
        });
    }

    /**
     * Getter alias
     *
     * @return alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Setter alias
     *
     * @param alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * Getter password
     *
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter password
     *
     * @param password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter host
     *
     * @return host
     */
    public String getHost() {
        return host;
    }

    /**
     * Setter host
     *
     * @param host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Getter port
     *
     * @return port
     */
    public int getPort() {
        return port;
    }

    /**
     * Setter port
     *
     * @param port to set
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Getter mails
     *
     * @return mails
     */
    public Collection<Mail> getMails() {
        return mails;
    }

    /**
     * Setter mails
     *
     * @param mails to set
     */
    public void setMails(Collection<Mail> mails) {
        this.mails = mails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ServeurMail that = (ServeurMail) o;
        return port == that.port &&
                Objects.equals(password, that.password) &&
                Objects.equals(host, that.host) &&
                Objects.equals(alias, that.alias);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), password, host, port, alias);
    }

    @Override
    public String toString() {
        return "ServeurMail{" +
                "password='" + password + '\'' +
                ", alias='" + alias + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", uuid=" + uuid +
                ", identifiant='" + identifiant + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }
}