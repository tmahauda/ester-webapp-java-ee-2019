package fr.univangers.esterwebapp.model;

import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.util.nameof.LangUtils;
import org.apache.log4j.Logger;
import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Modele abstrait qui représente un Utilisateur par
 * - Son identifiant unique
 * - Son rôle
 * - Sa première connexion au site
 */
@MappedSuperclass
//Rendre l'identifiant unique pour les utilisateurs => ne fonctionne pas
//@AttributeOverride(name = "identifiant", column = @Column(name="identifiant", nullable = false, unique = true))
public abstract class Utilisateur extends Model {

    private static Logger log = Logger.getLogger(Utilisateur.class);

    /**
     * Le role de l'utilisateur
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    protected Role role;

    /**
     * Est-ce la première connexion de l'utilisateur sur le site ?
     */
    @Column(name = "firstConnexion")
    protected boolean firstConnexion;

    /**
     * Constructeur par défaut pour Hibernate
     */
    public Utilisateur() {
        this(UUID.randomUUID().toString(), Role.ASSISTANT, false);
    }

    /**
     * Constructeur avec paramétres pour intialiser les attributs
     * @param identifiant
     * @param role
     * @param firstConnexion
     */
    public Utilisateur(String identifiant, Role role, boolean firstConnexion) {
        super(identifiant);
        this.role = role;
        log.info("Role : " + this.role);
        this.firstConnexion = firstConnexion;
        log.info("Première connexion : " + this.firstConnexion);
    }

    /**
     * Getter role
     *
     * @return role
     */
    public Role getRole() {
        return role;
    }

    /**
     * Setter role
     *
     * @param role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Getter isFirstConnexion
     *
     * @return isFirstConnexion
     */
    public boolean isFirstConnexion() {
        return firstConnexion;
    }

    /**
     * Setter isFirstConnexion
     *
     * @param isFirstConnexion to set
     */
    public void setFirstConnexion(boolean isFirstConnexion) {
        this.firstConnexion = isFirstConnexion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Utilisateur that = (Utilisateur) o;
        return firstConnexion == that.firstConnexion &&
                role == that.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), role, firstConnexion);
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "identifiant='" + identifiant + '\'' +
                ", role=" + role +
                ", firstConnexion=" + firstConnexion +
                ", uuid=" + uuid +
                ", created=" + created +
                ", updated=" + updated +
                '}';
    }

    @Override
    protected String toJSON(List<Class<? extends Model>> clazzFrom, String indentation) {
        String json = super.toJSON(clazzFrom, indentation);

        json += indentation + "\t\"" + LangUtils.nameOfProperty(this.getClass(), Utilisateur::getRole) + "\":\"" + this.getRole().toString() + "\",\n";
        json += indentation + "\t\"" + "firstConnexion" + "\":" + this.isFirstConnexion() + ",\n";

        return json;
    }
}