package fr.univangers.esterwebapp.formulaire.reference;

import fr.univangers.esterwebapp.model.reference.ImportDataFile;
import fr.univangers.esterwebapp.model.reference.ImportDataFileCSV;
import fr.univangers.esterwebapp.model.reference.ImportDataFileJSON;
import fr.univangers.esterwebapp.formulaire.Form;
import fr.univangers.esterwebapp.formulaire.ModelForm;
import fr.univangers.esterwebapp.model.modelenum.proposition.FileFormat;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

@Form
public class ImportDataReferenceForm extends ModelForm<ImportDataFile> {

    /**
     * Logger pour tracer les traitements du formulaire
     */
    private static Logger log = Logger.getLogger(ImportDataReferenceForm.class);

    /**
     * Nom du champ file dans le formulaire (mappage entre la back et le front)
     */
    public static final String FILE_INPUT = "fileInput";

    public ImportDataReferenceForm(HttpServletRequest request) {
        super(request);
    }

    @Override
    public ImportDataFile createModelFromRequest() {

        //On vérifie la présence du fichier
        Part filePart = null;
        try {
            filePart = request.getPart(FILE_INPUT);
            log.info("File part : " + filePart);
            if(filePart == null) {
               this.erreurs.put(FILE_INPUT, "Le fichier n'a pas été reconnu");
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            this.erreurs.put(FILE_INPUT, "Le fichier n'a pas été reconnu");
        }

        //Puis on vérifie le format du fichier
        if(filePart != null) {
            log.info("Name file : " + filePart.getSubmittedFileName());
            if(filePart.getSubmittedFileName().contains(FileFormat.JSON.getCode())) {
                log.info("File JSON");
                return new ImportDataFileJSON(filePart);
            } else if(filePart.getSubmittedFileName().contains(FileFormat.CSV.getCode())) {
                log.info("File CSV");
                return new ImportDataFileCSV(filePart);
            } else {
                log.info("File unknow");
                return null;
            }
        } else return null;
    }
}
