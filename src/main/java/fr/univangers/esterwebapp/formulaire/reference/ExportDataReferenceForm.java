package fr.univangers.esterwebapp.formulaire.reference;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.model.reference.ExportDataFile;
import fr.univangers.esterwebapp.model.reference.ExportDataFileCSV;
import fr.univangers.esterwebapp.model.reference.ExportDataFileJSON;
import fr.univangers.esterwebapp.formulaire.Form;
import fr.univangers.esterwebapp.formulaire.ModelForm;
import fr.univangers.esterwebapp.model.modelenum.proposition.FileFormat;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;

@Form
public class ExportDataReferenceForm<T extends Model> extends ModelForm<ExportDataFile<T>> {

    /**
     * Logger pour tracer les traitements du formulaire
     */
    private static Logger log = Logger.getLogger(ExportDataReferenceForm.class);

    /**
     * Nom du champ format fichier dans le formulaire (mappage entre la back et le front)
     */
    public static final String FORMAT_FILE_SELECT = "formatFileSelect";

    /**
     * Le model dont on doit exporter des données
     */
    private T model;

    /**
     * Le format de fichier : JSON ou CSV
     */
    private FileFormat fileFormat;

    public ExportDataReferenceForm(HttpServletRequest request, T model) {
        super(request);
        this.model = model;
    }

    @Override
    public ExportDataFile<T> createModelFromRequest() {

        String formatFile = this.getValueInput(FORMAT_FILE_SELECT);
        log.info("Format file : " + formatFile);
        if(Util.isNullOrEmpty(formatFile)) {
            this.erreurs.put(FORMAT_FILE_SELECT, "Le format de fichier n'est pas reconnu");
        }

        this.fileFormat = FileFormat.getProposition(formatFile);
        if(this.fileFormat == null) {
            this.erreurs.put(FORMAT_FILE_SELECT, "Le format de fichier n'est pas reconnu");
        }

        if(this.fileFormat == FileFormat.JSON) {
            return new ExportDataFileJSON<>(this.model);
        } else if(this.fileFormat == FileFormat.CSV) {
            return new ExportDataFileCSV<>(this.model);
        } else return null;
    }

    /**
     * Getter model
     *
     * @return model
     */
    public T getModel() {
        return model;
    }

    /**
     * Setter model
     *
     * @param model to set
     */
    public void setModel(T model) {
        this.model = model;
    }

    /**
     * Getter fileFormat
     *
     * @return fileFormat
     */
    public FileFormat getFileFormat() {
        return fileFormat;
    }

    /**
     * Setter fileFormat
     *
     * @param fileFormat to set
     */
    public void setFileFormat(FileFormat fileFormat) {
        this.fileFormat = fileFormat;
    }
}
