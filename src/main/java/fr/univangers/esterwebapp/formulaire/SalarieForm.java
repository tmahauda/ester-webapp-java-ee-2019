package fr.univangers.esterwebapp.formulaire;

import fr.univangers.esterwebapp.model.Entreprise;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.model.Travail;
import fr.univangers.esterwebapp.model.modelenum.proposition.Departement;
import fr.univangers.esterwebapp.model.modelenum.proposition.NAF;
import fr.univangers.esterwebapp.model.modelenum.proposition.PCS;
import fr.univangers.esterwebapp.model.modelenum.proposition.Region;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Age;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Sexe;
import fr.univangers.esterwebapp.service.EntrepriseService;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Form
public class SalarieForm extends ModelForm<Salarie> {

    private static Logger log = Logger.getLogger(SalarieForm.class);

    /**
     * La vue où se trouve le formulaire
     */
    public static final String VUE_FORM_SALARIE = "/jsp/formpatient.jsp";

    /**
     * Les différents champs caractérisant le salarié
     */
    public static final String SEXE_INPUT_H = "SexeInputH";
    public static final String SEXE_INPUT_F = "SexeInputF";
    public static final String SEXE_INPUT = "SexeInput";

    public static final String AGE_SELECT = "AgeSelect";
    public static final String DEPARTEMENT_INPUT = "DepartementInput";
    public static final String REGION_INPUT = "RegionInput";
    public static final String CODE_NAF_INPUT = "CodeNAFInput";
    public static final String CODE_PCS_INPUT = "CodePCSInput";
    public static final String ENTREPRISE_INPUT = "EntrepriseInput";

    public static final String ENTREPRISE = "entreprise";


    public SalarieForm(HttpServletRequest request) {
        super(request);
    }

    @Override
    public boolean updateModelFromRequest(Salarie salarie) {
        String sexe = this.getValueInput(SEXE_INPUT);
        log.info("Sexe : " + sexe);
        if(Util.isNullOrEmpty(sexe)) {
            this.erreurs.put(SEXE_INPUT, "Le sexe n'est pas renseigné !");
        }

        String age = this.getValueInput(AGE_SELECT);
        log.info("Age : " + age);
        if(Util.isNullOrEmpty(age)) {
            this.erreurs.put(AGE_SELECT, "L'âge n'est pas renseigné !");
        }

        String departement = this.getValueInput(DEPARTEMENT_INPUT);
        log.info("Département : " + departement);

        String region = this.getValueInput(REGION_INPUT);
        log.info("Région : " + region);

        String codeNAF = this.getValueInput(CODE_NAF_INPUT);
        log.info("Code NAF : " + codeNAF);

        String codePCS = this.getValueInput(CODE_PCS_INPUT);
        log.info("Code PCS : " + codePCS);

        String entrepriseID = this.getValueInput(ENTREPRISE_INPUT);
        log.info("ID Entreprise : " + entrepriseID);

        //Si le formulaire est valide alors on peut modifier les infos du salarié
        if(this.isValid()) {
            salarie.setSexe(Sexe.getProposition(sexe));
            Age a = Age.getProposition(age);
            salarie.setAge(a);
           /* if(a != null) {
                salarie.setNaisInf(a.getMinAge());
                salarie.setNaisSup(a.getMaxAge());
            }*/
            if (salarie.getTravail() == null) salarie.setTravail(new Travail());
            salarie.getTravail().setDepartementActivite(Departement.getProposition(departement));
            salarie.getTravail().setRegionActivite(Region.getProposition(region));
            salarie.getTravail().setCodeNAF(NAF.getProposition(codeNAF));
            salarie.getTravail().setCodePCS(PCS.getProposition(codePCS));
            if(!entrepriseID.equals("Autres..."))
                salarie.setEntreprise(EntrepriseService.getInstance().readByName(entrepriseID));
            //On ne réaffiche plus le formulaire
            salarie.setFirstConnexion(false);
        }

        //Si le modèle est crée alors un message succés est envoyé.
        //Sinon c'est un échec
        if(this.isValid()) {
            this.resultat = "Modifications effectuées avec succès";
        } else {
            this.resultat = "Echec lors de la modification";
        }

        return this.isValid();
    }

    public void getEntreprise(){
        List<Entreprise> entrepriseList = new ArrayList<>(EntrepriseService.getInstance().read());
        Entreprise autre = new Entreprise();
        autre.setIdentifiant("Autres...");
        autre.setNom("Autres...");
        entrepriseList.add(autre);
        log.info("List d'entreprise : "+entrepriseList);
        request.setAttribute(ENTREPRISE, entrepriseList);
    }
}
