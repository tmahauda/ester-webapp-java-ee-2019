package fr.univangers.esterwebapp.filter;

import fr.univangers.esterwebapp.model.Entreprise;
import fr.univangers.esterwebapp.servlet.EntrepriseServlet;

import javax.servlet.annotation.WebFilter;

/**
 * Filtre qui permet de s'assurer que l'utilisateur est bien une entreprise
 * Si entreprise alors il peut se connecter et continuer
 * Sinon il revient sur la page d'accueil
 */
@WebFilter(filterName = "EntrepriseFilter", urlPatterns = {EntrepriseFilter.URL_ENTREPRISE})
public class EntrepriseFilter extends UtilisateurFilter<Entreprise> {

    public static final String URL_ENTREPRISE = EntrepriseServlet.URL_ENTREPRISE + "/*";

    public EntrepriseFilter() {
        super(Entreprise.class);
    }
}