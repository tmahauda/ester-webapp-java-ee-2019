package fr.univangers.esterwebapp.filter;

import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.servlet.SalarieServlet;

import javax.servlet.annotation.WebFilter;

/**
 * Filtre qui permet de s'assurer que l'utilisateur est bien un salarié
 * Si salarié alors il peut se connecter et continuer
 * Sinon il revient sur la page d'accueil
 */
@WebFilter(filterName = "SalarieFilter", urlPatterns = {SalarieFilter.URL_SALARIE})
public class SalarieFilter extends UtilisateurFilter<Salarie> {

    public static final String URL_SALARIE = SalarieServlet.URL_SALARIE + "/*";

    public SalarieFilter() {
        super(Salarie.class);
    }
}