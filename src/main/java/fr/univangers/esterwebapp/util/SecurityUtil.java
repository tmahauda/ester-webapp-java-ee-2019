package fr.univangers.esterwebapp.util;

import at.favre.lib.crypto.bcrypt.BCrypt;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.service.SalarieService;
import org.apache.log4j.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class SecurityUtil {

    private static Logger log = Logger.getLogger(SecurityUtil.class);
    private static final String BLOWFISH = "Blowfish";
    private static final String CLE_SECRETE = "clesecrete";

    private SecurityUtil() {}

    public boolean verifMdp(String motPasse) {
        boolean verifLettre = false;
        boolean verifChiffre = false;

        for(Character current : motPasse.toCharArray()) {
            // Si le caractère courant est un chiffre, alors on toggle à true la vérification des chiffres
            if(Character.isDigit(current) && !verifChiffre) {
                verifChiffre = true;
            }


            // Si le caractère courant est une lettre, alors on toggle à true la vérification des lettres
            if(Character.isLetter(current) && !verifLettre) {
                verifLettre = true;
            }
        }

        log.info("Mot de passe contient des chiffres : " + verifChiffre);
        log.info("Mot de passe contient des lettres : " + verifLettre);

        // On retourne True si le mdp est valide, False sinon
        return verifChiffre && verifLettre;
    }

    public static String createIdentifiant() {
        String nouveauIdentifiant = null;
        String dernierIdentifiant = null;
        Salarie salarie = SalarieService.getInstance().readLast();
        if (salarie != null)
            dernierIdentifiant = salarie.getIdentifiant();
        if (Util.isNullOrEmpty(dernierIdentifiant))
        {
            try {
                nouveauIdentifiant= chiffrer("1");
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        else{
            try {
                nouveauIdentifiant = chiffrer(
                        String.valueOf(Integer.parseInt(dechiffrer(dernierIdentifiant)) + 1));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }

        }
        return nouveauIdentifiant;
    }

    public static String createIdentifiant(int i) {
        String nouveauIdentifiant = null;
        try {
            nouveauIdentifiant= chiffrer(String.valueOf(i));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return nouveauIdentifiant;
    }

    public static String chiffrerMotPasse(String motPasse) {
        return BCrypt.withDefaults().hashToString(8, motPasse.toCharArray());
    }

    public static boolean verifierMotPasse(String motPasse, String bcryptHash) {
        return BCrypt.verifyer().verify(motPasse.toCharArray(), bcryptHash).verified;
    }

    public static String chiffrer(String dernierIdentifiant) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {

        byte[] keyData = (CLE_SECRETE).getBytes();
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, BLOWFISH);

        //creer un chiffrement basé sur blowfish
        Cipher cipher = Cipher.getInstance(BLOWFISH);

        //initialiser le chiffreur avec la cle secrete
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);

        //encrypt message
        byte[] encrypted = cipher.doFinal(dernierIdentifiant.getBytes());

        return Base64.getEncoder().encodeToString(encrypted);
    }

    public static String dechiffrer(String encryptedData) throws BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        byte[] keyData = (CLE_SECRETE).getBytes();
        SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, BLOWFISH);
        Cipher cipher = Cipher.getInstance(BLOWFISH);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
        byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(encryptedData));
        return new String(decrypted);
    }
}