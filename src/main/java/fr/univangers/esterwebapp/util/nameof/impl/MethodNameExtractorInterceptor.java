package fr.univangers.esterwebapp.util.nameof.impl;

import net.bytebuddy.implementation.bind.annotation.Origin;
import net.bytebuddy.implementation.bind.annotation.RuntimeType;

import java.lang.reflect.Method;

public class MethodNameExtractorInterceptor {

    private static final ThreadLocal<String> currentExtractedMethodName = new ThreadLocal<>();

    private MethodNameExtractorInterceptor() {}

    @RuntimeType
    public static Object intercept(@Origin Method method) {
        currentExtractedMethodName.set(getMethodName(method));

        if (method.getReturnType() == byte.class) {
            return (byte) 0;
        }
        if (method.getReturnType() == int.class) {
            return 0;
        }
        if (method.getReturnType() == long.class) {
            return (long) 0;
        }
        if (method.getReturnType() == char.class) {
            return (char) 0;
        }
        if (method.getReturnType() == short.class) {
            return (short) 0;
        }
        return null;
    }

    private static String getMethodName(Method method) {
        boolean canBeGetter = method.getParameterTypes().length == 0
                && method.getReturnType() != null;

        if (canBeGetter) {
            return method.getName();
        }

        throw new RuntimeException("Only property getter methods are expected to be passed");
    }
    
    public static String extractMethodName() {
        String methodName = currentExtractedMethodName.get();
        currentExtractedMethodName.remove();
        return methodName;
    }
}