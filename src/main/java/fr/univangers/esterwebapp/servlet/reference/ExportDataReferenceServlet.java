package fr.univangers.esterwebapp.servlet.reference;

import fr.univangers.esterwebapp.model.Model;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.reference.ExportDataFile;
import fr.univangers.esterwebapp.formulaire.reference.ExportDataReferenceForm;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.model.modelenum.proposition.FileFormat;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

public class ExportDataReferenceServlet<T extends Model> extends DataReferenceServlet<T> {

    private static Logger log = Logger.getLogger(ExportDataReferenceServlet.class);

    public ExportDataReferenceServlet(T model) {
        super(model);
    }

    @Override
    public boolean execute(HttpServletRequest request, HttpServletResponse response) {
        ExportDataReferenceForm<T> exportDataReferenceForm = new ExportDataReferenceForm<>(request, this.model);
        ExportDataFile<T> exportDataFile = exportDataReferenceForm.createModelFromRequest();

        Servlet.writeUserInSession(UtilisateurEsterService.getInstance().readById(Servlet.readUserInSession(request).getUuid()),false, request);

        if(exportDataFile != null) {
            boolean write = exportDataFile.write();
            if(write) {
                //On récupère le contenu du fichier = model
                String records = exportDataFile.getRecords();
                if(!Util.isNullOrEmpty(records)) {
                    FileFormat format = exportDataReferenceForm.getFileFormat();

                    //Modifie les headers de la réponse
                    response.setContentType(format.getNom());
                    response.setContentLength(records.length());

                    //Afficher la fenetre de téléchargement
                    String headerKey = "Content-Disposition";
                    String headerValue = String.format("attachment; filename=\"%s.%s\"", this.model.getIdentifiant(), format.getCode());
                    response.setHeader(headerKey, headerValue);

                    //Ecriture du contenu dans la réponse
                    try {
                        OutputStream outStream = response.getOutputStream();
                        outStream.write(records.getBytes());
                        outStream.close();
                        return true;
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        Servlet.ATT_MSG_ERROR.write("Erreur lors de l'écriture du fichier dans la réponse", request);
                        return false;
                    }
                } else {
                    Servlet.ATT_MSG_ERROR.write("Aucun contenu à exporter", request);
                    return false;
                }
            } else {
                Servlet.ATT_MSG_ERROR.write("Erreur lors de l'écriture du contenu dans le fichier", request);
                return false;
            }
        } else {
            Servlet.ATT_MSG_ERROR.write("Le format du fichier n'est pas reconnu. Uniquement JSON ou CSV", request);
            return false;
        }
    }
}
