package fr.univangers.esterwebapp.servlet.reference;

import fr.univangers.esterwebapp.model.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class DataReferenceServlet<T extends Model> {

    protected T model;

    public DataReferenceServlet(T model) {
        this.model = model;
    }

    public abstract boolean execute(HttpServletRequest request, HttpServletResponse response);
}
