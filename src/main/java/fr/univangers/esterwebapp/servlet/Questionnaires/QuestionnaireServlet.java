package fr.univangers.esterwebapp.servlet.Questionnaires;

import fr.univangers.esterwebapp.formulaire.Questionnaire.ReponseQuestionnaireForm;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.model.*;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@WebServlet(name = "QuestionnaireServlet", urlPatterns = {QuestionnaireServlet.URL_QUESTIONNAIRE})
public class QuestionnaireServlet extends HttpServlet {

    private static final Logger log = Logger.getLogger(QuestionnaireServlet.class);

    public static final String URL_QUESTIONNAIRE = "/salarie/questionnaire";
    public static final String VUE_QUESTIONNAIRE = "/jsp/Questionnaries/afficheQuestionnaire.jsp";
    public static final String QUESTIONNAIRE = "Questionnaire";
    public static final String QUESTIONNAIRES_SELECT = "QuestionnairesSelect";
    public static final String QUESTIONNAIRE_INPUT = "nameQuestionnaire";
    public static final String NB_QUESTIONS_INPUT = "nbQuestions";

    public static final String QUESTION_INPUT = ReponseQuestionnaireForm.QUESTION_INPUT;
    public static final String REPONSE_INPUT = ReponseQuestionnaireForm.REPONSE_INPUT;


    public boolean errorMessages(HttpServletRequest request, String msgError){
        Servlet.ATT_MSG_ERROR.write(ReponseQuestionnaireForm.MSG_ERREUR, request);
        log.error(msgError);
        return false;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Questionnaire");

        //On récupère l'identifiant du questionnaire coté front
        String nameQuestionnaire = request.getParameter(QUESTIONNAIRE_INPUT);
        log.info("Nom questionnaire : " + nameQuestionnaire);
        if(Util.isNullOrEmpty(nameQuestionnaire)) {
            errorMessages( request,"L'identifiant du questionnaire est vide");
        }

        //On récupère le questionnaire coté back
        Questionnaire questionnaire = QuestionnaireService.getInstance().readByName(nameQuestionnaire);
        log.info("Questionnaire : " + questionnaire);
        if(questionnaire == null) {
            errorMessages(request, "Le questionnaire n'existe pas dans la base de données");
        }

        //On récupère le nombre de questions du questionnaire coté front
        String nombreQuestions = request.getParameter(NB_QUESTIONS_INPUT);
        log.info("Nombre de questions str : " + nombreQuestions);
        if(Util.isNullOrEmpty(nombreQuestions)) {
            errorMessages( request, "Le nombre de questions est vide");
        }

        int nbQuestions = 0;
        try {
            nbQuestions = Integer.parseInt(nombreQuestions);
            log.info("Nombre de questions int : " + nbQuestions);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }

        Salarie salarie = Servlet.readUserInSession(request);
        log.info("Salarié : " + salarie);

        if(questionnaire != null && salarie != null && nbQuestions > 0) {

            //On crée un résultat pour ce questionnaire associé au salarié
            Resultat resultat = new Resultat(null, new Date(), new Date(), 0, questionnaire, salarie.getCreateurSalarie(), salarie);

            ReponseQuestionnaireForm form = new ReponseQuestionnaireForm(request, questionnaire, salarie, resultat);
            boolean valid = form.getQuestionnaryElements(nbQuestions);

            if(valid) {
                form.saveAllUpdating();
            }
        }

        Servlet.writeUserInSession(salarie, false, request);
        Servlet.dispatch(request, response, VUE_QUESTIONNAIRE);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get Questionnaire");

        String nameQuestionnaire = request.getParameter(QUESTIONNAIRES_SELECT);
        log.info("Nom Questionnaire : " + nameQuestionnaire);

        if(!Util.isNullOrEmpty(nameQuestionnaire)) {
            Questionnaire questionnaire = QuestionnaireService.getInstance().readByName(nameQuestionnaire);
            log.info("Questionnaire : " + questionnaire);
            String json = questionnaire.toJSONFormat();
            log.info("JSON Questionnaire : " + json);
            request.setAttribute(QUESTIONNAIRE, json);
            request.setAttribute(QUESTIONNAIRE_INPUT, nameQuestionnaire);
        }

        Servlet.dispatch(request, response, VUE_QUESTIONNAIRE);
    }
}