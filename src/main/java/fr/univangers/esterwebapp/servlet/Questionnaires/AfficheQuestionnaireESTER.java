package fr.univangers.esterwebapp.servlet.Questionnaires;

import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.Utilisateur;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

@WebServlet(name = "AfficheQuestionnaireESTERServlet", urlPatterns = {"/"+AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER})
public class AfficheQuestionnaireESTER extends HttpServlet {

    /**
     * Logger pour tracer les traitements de la servlet
     */
    private static Logger log = Logger.getLogger(AfficheQuestionnaireESTER.class);

    public static final String URL_AFFICHE_QUESTIONNAIRE_ESTER = "afficheQuestionnaireESTER";
    public static final String VUE_AFFICHE_QUESTIONNAIRE_ESTER = "/jsp/Questionnaries/afficheQuestionnaireESTER.jsp";
    public static final String VUE_UTILISATEUR = "/jsp/utilisateur.jsp";
    public static final String VUE_ENTREPRISE = "/jsp/entreprise.jsp";

    public static final String QUESTIONNAIRES = "questionnaires";

    public static final String QUESTIONNAIRES_SELECT = "QuestionnairesSelect";
    public static final String QUESTIONNAIRE = "Questionnaire";
    public static final String QUESTIONNAIRE_NAME = "QuestionnaireName";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Questionnaire");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get AfficheQuestionnaireEster");

        Utilisateur user= Servlet.readUserInSession(request);
        //On recupère les questionnaires
        Collection<Questionnaire> questionnaires = QuestionnaireService.getInstance().read();
        log.info("List questionnaires : " + questionnaires);

        request.setAttribute(QUESTIONNAIRES, questionnaires);

        String idQuestionnaire = request.getParameter(QUESTIONNAIRES_SELECT);
        log.info("ID Questionnaire : " + idQuestionnaire);

        if(!Util.isNullOrEmpty(idQuestionnaire)) {
            Questionnaire questionnaire = QuestionnaireService.getInstance().readById(idQuestionnaire);
            log.info("Questionnaire : " + questionnaire);
            String json = questionnaire.toJSONFormat();
            log.info("JSON Questionnaire : " + json);
            request.setAttribute(QUESTIONNAIRE, json);

            String nameQuestionnaire = questionnaire.getNom();
            log.info("Nom Questionnaire : " + nameQuestionnaire);
            request.setAttribute(QUESTIONNAIRE_NAME, nameQuestionnaire);

        }
        if(user.getRole()== Role.ENTREPRISE){
            Servlet.dispatch(request, response, VUE_ENTREPRISE);
        }else{
            Servlet.dispatch(request, response, VUE_UTILISATEUR);
        }


    }
}