package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.Reponse;
import fr.univangers.esterwebapp.model.Resultat;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.service.ResultatService;
import fr.univangers.esterwebapp.service.SalarieService;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@WebServlet(name = "ResultatServlet", urlPatterns = {ResultatServlet.URL_RESULTAT})
public class ResultatServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(ResultatServlet.class);

    public static final String URL_RESULTAT = "/resultat";
    public static final String VUE_RESULTAT = "/jsp/resultat.jsp";
    public static final String QUESTIONNAIRE_RESULTAT = "questionnaire";
    public static final String QUESTION_RPE_RESULTAT = "questionRpe";
    public static final String POURCENTAGE_SCORE = "pourcentagesScore";
    public static final String POURCENTAGE_RPE = "pourcentagesRPE";


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Resultat");
        Questionnaire questionnaire = QuestionnaireService.getInstance().readById("evalRiskTMS_SMS-MSChronique");
        String identifiantsalarie = request.getParameter("identifiantSalarie");
        Resultat resultat = ResultatService.getInstance().resultatSalarieQuestionnaire(questionnaire, SalarieService.getInstance().readById(identifiantsalarie));
        log.info("Resultat Salarie"+resultat);

        //Calcule son score
        int score = 0;
        String reponseRpe="";
        ArrayList<String> reponsesQuestions= new ArrayList<>();
        for(Reponse reponse:resultat.getReponseParQuestion()) {
            score += reponse.getScoreReponse();
            reponsesQuestions.add(reponse.getValeurReponse());
            if(reponse.getQuestion().getIntituleQuestion().compareTo("Comment évaluez-vous l'intensité des efforts physiques de votre travail au cours d'une journée de travail ?")==0)
                reponseRpe = reponse.getValeurReponse();
        }
        log.info("Score Salarie "+score);
        request.getSession(false).setAttribute("score",score);
        //sa reponse rpe
        log.info("Reponse RPE"+reponseRpe);
        request.getSession(false).setAttribute("reponseRPE",reponseRpe);
        //placer sa reponse aux questions dans les charts
        log.info("Reponses du salarie "+reponsesQuestions);
        request.getSession(false).setAttribute("reponsesQuestions",reponsesQuestions);
        this.doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get Resultat");
        //Recupérer les données de références
        Questionnaire questionnaire = QuestionnaireService.getInstance().readById("evalRiskTMS_SMS-MSChronique");
        log.info(" Questionnaire resultatServlet "+questionnaire);
        request.getSession(false).setAttribute(QUESTIONNAIRE_RESULTAT,questionnaire.getIdentifiant());

        Collection<Salarie> salariesRepondu = questionnaire.getSalariesRepondus();
        ArrayList<String> identifiantsSalariesRepondus = new ArrayList<>();
        for(Salarie salarie:salariesRepondu) {
            identifiantsSalariesRepondus.add(salarie.getIdentifiant());
        }
        request.getSession(false).setAttribute("listSalarie", identifiantsSalariesRepondus);

        /*Question question = QuestionService.getInstance().readByIntitule("Comment évaluez-vous l''intensité des efforts physiques de votre travail au cours d''une journée de travail ?");
        log.info("Question RPE: "+question);
        request.getSession(false).setAttribute(QUESTION_RPE_RESULTAT,question);*/

           /* //calculer les scores pour l'histo
        Map<String,Double> pourcentagesScore = ResultatRefService.getInstance().pourcentageScore(questionnaire);
        request.getSession(false).setAttribute(POURCENTAGE_SCORE,pourcentagesScore);*/

        //calculer pourcentage RPE
        /*Map<String,Double> pourcentageRpe = ResultatRefService.getInstance().pourcentageRpe(questionnaire,question);
        request.getSession(false).setAttribute(POURCENTAGE_RPE,pourcentageRpe);*/
        Servlet.dispatch(request, response, VUE_RESULTAT);
    }
}
