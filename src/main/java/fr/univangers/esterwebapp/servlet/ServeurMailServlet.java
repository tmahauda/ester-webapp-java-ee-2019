package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.model.ServeurMail;
import fr.univangers.esterwebapp.service.ServeurMailService;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;

public class ServeurMailServlet extends HttpServlet {

    /**
     * Logger pour tracer les traitements de la servlet
     */
    private static Logger log = Logger.getLogger(ServeurMailServlet.class);

    /**
     * Le serveur de mail ESTER
     */
    public static final String SERVEUR_MAIL = "serveurMail";

    /**
     * URL pour configurer le serveur de mail
     */
    public static final String URL_SERVEUR_MAIL = "serveurMail";

    /**
     * La vue pour configurer le serveur de mail
     */
    public static final String VUE_SERVEUR_MAIL = "/jsp/serveurMail.jsp";

    /**
     * Nom du champ identifiant dans le formulaire (mappage entre la back et le front)
     */
    public static final String IDENTIFIANT_INPUT = "identifiantInput";

    /**
     * Nom du champ mail dans le formulaire (mappage entre la back et le front)
     */
    public static final String MAIL_INPUT = "mailInput";

    /**
     * Nom du champ alias dans le formulaire (mappage entre la back et le front)
     */
    public static final String ALIAS_INPUT = "aliasInput";

    /**
     * Nom du champ mot de passe dans le formulaire (mappage entre la back et le front)
     */
    public static final String MOTPASSE_INPUT = "passwordInput";

    /**
     * Nom du champ host dans le formulaire (mappage entre la back et le front)
     */
    public static final String HOST_INPUT = "hostInput";

    /**
     * Nom du champ host dans le formulaire (mappage entre la back et le front)
     */
    public static final String PORT_INPUT = "portInput";


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post ServeurMail");

        boolean valid = true;

        String identifiant = request.getParameter(IDENTIFIANT_INPUT);
        log.info("Identifiant : " + identifiant);
        if(Util.isNullOrEmpty(identifiant)) {
            valid = false;
        }

        String email = request.getParameter(MAIL_INPUT);
        log.info("Email : " + email);
        if(Util.isNullOrEmpty(email)) {
            Servlet.ATT_MSG_WARNING.write("Vous devez renseigner un mail !", request);
            valid = false;
        }

        String alias = request.getParameter(ALIAS_INPUT);
        log.info("Alias : " + alias);
        if(Util.isNullOrEmpty(alias)) {
            Servlet.ATT_MSG_WARNING.write("Vous devez renseigner un alias !", request);
            valid = false;
        }

        String password = request.getParameter(MOTPASSE_INPUT);
        log.info("Password : " + password);
        if(Util.isNullOrEmpty(password)) {
            Servlet.ATT_MSG_WARNING.write("Vous devez renseigner un mot de passe !", request);
            valid = false;
        }

        String host = request.getParameter(HOST_INPUT);
        log.info("Host : " + host);
        if(Util.isNullOrEmpty(host)) {
            Servlet.ATT_MSG_WARNING.write("Vous devez renseigner un host !", request);
            valid = false;
        }

        int numeroPort = 0;
        String port = request.getParameter(PORT_INPUT);
        log.info("Port : " + port);
        if(Util.isNullOrEmpty(port)) {
            Servlet.ATT_MSG_WARNING.write("Vous devez renseigner un port !", request);
            valid = false;
        } else {
            try {
                numeroPort = Integer.parseInt(port);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                valid = false;
            }
        }

        if(valid) {
            ServeurMail serveurMail = ServeurMailService.getInstance().readById(identifiant);
            if(serveurMail != null) {
                serveurMail.setIdentifiant(email);
                serveurMail.setAlias(alias);
                serveurMail.setPassword(password);
                serveurMail.setHost(host);
                serveurMail.setPort(numeroPort);

                boolean save = ServeurMailService.getInstance().save(serveurMail);
                if(save) {
                    Servlet.ATT_MSG_SUCCESS.write("Serveur de mail enregistré", request);
                } else {
                    Servlet.ATT_MSG_ERROR.write("Serveur de mail non enregistré", request);
                }
            } else {
                Servlet.ATT_MSG_ERROR.write("Serveur de mail non enregistré", request);
            }
        }

        this.doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get ServeurMail");

        //On enregistre le serveur dans la requete
        Collection<ServeurMail> serveurMails = ServeurMailService.getInstance().read();
        if(!serveurMails.isEmpty()) {
            request.setAttribute(SERVEUR_MAIL, serveurMails.toArray()[0]);
        }
    }
}