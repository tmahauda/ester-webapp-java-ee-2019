package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.formulaire.ModelForm;
import fr.univangers.esterwebapp.model.Utilisateur;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public enum Servlet {

    ATT_MSG_WARNING("Warning"),
    ATT_MSG_SUCCESS("Success"),
    ATT_MSG_ERROR("Error");

    private static Logger log = Logger.getLogger(Servlet.class);

    public static final String VUE_ALERT = "/jsp/UtilitiesToImport/alert.jsp";
    public static final String MESSAGE_ALERT = "Message";
    public static final String USER = "Utilisateur";
    public static final String PARAM_PAGE = "page";
    public static final String FORM = "form";

    private String type;

    Servlet(String type) {
        this.type = type;
    }

    public static boolean dispatch(HttpServletRequest request, HttpServletResponse response, String view) {
        try {
            request.getRequestDispatcher(view).forward(request, response);
            log.info("Dispatch to : " + view);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    public void write(String message, HttpServletRequest request) {
        request.setAttribute(MESSAGE_ALERT, true);
        request.setAttribute(this.type, message);
    }

    public static void writeUserInSession(Utilisateur user, boolean create, HttpServletRequest request) {
        request.getSession(create).setAttribute(USER, user);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Utilisateur> T readUserInSession(HttpServletRequest request) {
        return (T)request.getSession(false).getAttribute(USER);
    }

    public static void writeFormInRequest(ModelForm modelForm, HttpServletRequest request) {
        request.setAttribute(FORM, modelForm);
    }

    /**
     * Getter type
     *
     * @return type
     */
    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Servlet{" +
                "type='" + type + '\'' +
                '}';
    }
}