package fr.univangers.esterwebapp.servlet;

import fr.univangers.esterwebapp.formulaire.SalarieForm;
import fr.univangers.esterwebapp.model.Entreprise;
import fr.univangers.esterwebapp.model.Salarie;
import fr.univangers.esterwebapp.service.EntrepriseService;
import fr.univangers.esterwebapp.service.SalarieService;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "SalarieServlet", urlPatterns = {SalarieServlet.URL_SALARIE})
public class SalarieServlet extends HttpServlet {

    private static Logger log = Logger.getLogger(SalarieServlet.class);

    public static final String URL_SALARIE = "/salarie";
    public static final String VUE_SALARIE = "/jsp/salarie.jsp";
    public static final String ENTREPRISE = SalarieForm.ENTREPRISE;



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post Salarie");

        Salarie salarie = Servlet.readUserInSession(request);
        log.info("Salarié before : " + salarie);

        SalarieForm salarieForm = new SalarieForm(request);
        boolean update = salarieForm.updateModelFromRequest(salarie);
        log.info("Salarié after : " + salarie);

        if(update) {
            update = SalarieService.getInstance().save(salarie);
            if(update) {
                Servlet.writeUserInSession(salarie, false, request);
                Servlet.ATT_MSG_SUCCESS.write("Modifications effectuées avec succès", request);
            } else {
                Servlet.ATT_MSG_ERROR.write("Echec lors de l'enregistrement", request);
            }
        } else {
            Servlet.ATT_MSG_WARNING.write("Echec lors de la modification", request);
        }

        Servlet.writeFormInRequest(salarieForm, request);
        this.doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get Salarie");

        //On enregistre l'utilisateur dans la requete pour le formulaire
        Salarie salarie = Servlet.readUserInSession(request);
        request.setAttribute(ParametresConnexionServlet.UTILISATEUR, salarie);
        SalarieForm salarieForm = new SalarieForm(request);
        salarieForm.getEntreprise();
        Servlet.dispatch(request, response, VUE_SALARIE);
    }
}