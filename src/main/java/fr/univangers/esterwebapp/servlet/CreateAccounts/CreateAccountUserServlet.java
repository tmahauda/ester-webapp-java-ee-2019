package fr.univangers.esterwebapp.servlet.CreateAccounts;

import fr.univangers.esterwebapp.service.MailService;
import fr.univangers.esterwebapp.servlet.Servlet;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CreateAccountUserServlet extends HttpServlet {

    /**
     * Logger pour tracer les traitements de la servlet
     */
    private static Logger log = Logger.getLogger(CreateAccountUserServlet.class);

    public static final String URL_CREATE_ACCOUNT_USER = "createAccountUser";
    public static final String VUE_CREATE_ACCOUNT_USER = "/jsp/CreateAccounts/createAccountUser.jsp";

    public static final String EMAIL_INPUT = "EmailInput";
    public static final String COMPTE_SELECT = "CompteSelect";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post CreateAccountUserServlet");

        String email = request.getParameter(EMAIL_INPUT);
        log.info("Email : " + email);

        String creationCompte = request.getParameter(COMPTE_SELECT);
        log.info("Compte créé : " + creationCompte);

        String url = request.getRequestURL().toString();
        url = url.substring(0, url.length() - "/utilisateur".length()) + NewAccountUserServlet.URL_NEW_ACCOUNT_USER;
        log.info("URL = " + url);
        boolean send = MailService.getInstance().sendCreationCompte(email, url, creationCompte);

        //Si le mail est bel et bien envoyé
        if(send) {
            log.info("Mail envoyé à " + email);
            String message = "Un mail a été envoyé à " + email;
            Servlet.ATT_MSG_SUCCESS.write(message, request);
        }
        else {
            log.error("Problème survenu lors de l'envoi du mail à " + email);
            String message = "Un problème est survenu lors de l'envoi du mail à " + email + ". Veuillez réessayer plus tard.";
            Servlet.ATT_MSG_ERROR.write(message, request);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get CreateAccountUserServlet");
    }
}