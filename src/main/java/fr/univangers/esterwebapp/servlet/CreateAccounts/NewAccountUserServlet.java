package fr.univangers.esterwebapp.servlet.CreateAccounts;

import fr.univangers.esterwebapp.dao.mongodb.UtilisateurEsterDAO;
import fr.univangers.esterwebapp.model.Mail;
import fr.univangers.esterwebapp.model.Questionnaire;
import fr.univangers.esterwebapp.model.UtilisateurEster;
import fr.univangers.esterwebapp.model.modelenum.utilisateur.Role;
import fr.univangers.esterwebapp.service.MailService;
import fr.univangers.esterwebapp.service.QuestionnaireService;
import fr.univangers.esterwebapp.service.UtilisateurEsterService;
import fr.univangers.esterwebapp.servlet.ConnexionServlet;
import fr.univangers.esterwebapp.servlet.PasswordServlet;
import fr.univangers.esterwebapp.servlet.Servlet;
import fr.univangers.esterwebapp.util.Util;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static fr.univangers.esterwebapp.servlet.Questionnaires.GenerateurQuestionnaireServlet.EVAL_RISK_TMS;
import static fr.univangers.esterwebapp.servlet.Questionnaires.GenerateurQuestionnaireServlet.EVAL_RISK_TMS_LOMBALGIE;

@WebServlet(name = "NewAccountUserServlet", urlPatterns = {NewAccountUserServlet.URL_NEW_ACCOUNT_USER})
public class NewAccountUserServlet extends PasswordServlet {

    private static Logger log = Logger.getLogger(String.valueOf(CreateAccountUserServlet.class));

    public static final String URL_NEW_ACCOUNT_USER = "/newAccountUser";
    public static final String VUE_NEW_ACCOUNT_USER = "/jsp/CreateAccounts/newAccountUser.jsp";

    public static final String TOKEN_INPUT = "mail";
    public static final String TOKEN_VALUE = "ValueToken";
    public static final String ROLE_VALUE = "role";
    public static final String NEW_MOTPASSE_INPUT = "InputNewPassword";
    public static final String CONFIRM_MOTPASSE_INPUT = "InputConfirmPassword";
    public static final String NEW_MOTPASSE_VALUE = "ValueNewPassword";
    public static final String CONFIRM_MOTPASSE_VALUE = "ValueConfirmPassword";

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do post CreationUtilisateur");

        String vue = VUE_NEW_ACCOUNT_USER;

        String token = request.getParameter(TOKEN_INPUT);
        request.setAttribute(TOKEN_VALUE, token);
        log.info("Token : " + token);

        //Si le token existe
        if(!Util.isNullOrEmpty(token)) {

            UUID uuid = null;
            try {
                uuid = UUID.fromString(token);
                log.info("UUID : " + uuid);
            } catch (Exception e) {
                log.error(e.getMessage());
                uuid = null;
            }

            //Si le token est bien un uuid
            if (uuid != null) {

                Mail mail = MailService.getInstance().readById(uuid);
                log.info("Mail : " + mail);

                //Si il y a bien une demande de réinitialisation
                if (mail != null) {

                    String newPassword = request.getParameter(NEW_MOTPASSE_INPUT);
                    request.setAttribute(NEW_MOTPASSE_VALUE, newPassword);
                    log.info("Password : " + newPassword);

                    String confirmPassword = request.getParameter(CONFIRM_MOTPASSE_INPUT);
                    request.setAttribute(CONFIRM_MOTPASSE_VALUE, confirmPassword);
                    log.info("Confirmation password : " + confirmPassword);

                    String email = mail.getIdentifiant();

                    String stringRole = request.getParameter(ROLE_VALUE);
                    Role role = null;

                    switch (stringRole) {
                        case "Administrateur":
                            role = Role.ADMIN;
                            break;
                        case "Médecin":
                            role = Role.MEDECIN;
                            break;
                        case "Infirmier":
                            role = Role.INFIRMIER;
                            break;
                        case "Préventeur":
                            role = Role.PREVENTEUR;
                            break;
                        case "Assistant":
                            role = Role.ASSISTANT;
                            break;
                        default:
                    }
                    log.info("Role : " + role);

                    //Les mots de passe sont bien égaux
                    if (newPassword.equals(confirmPassword)) {

                        UtilisateurEster user = new UtilisateurEster(email,"", email, newPassword, role, true);
                        log.info("User : " + user);

                        UtilisateurEsterDAO.getInstance().create(user);
                        String message = "Création du compte réussie";
                        Servlet.ATT_MSG_SUCCESS.write(message, request);

                        if(user.getRole() == Role.ADMIN){
                                Collection<Questionnaire> questionnairesList = QuestionnaireService.getInstance().read();
                                user.setQuestionnairesAcces(questionnairesList);
                            }
                            else if(user.getRole() == Role.MEDECIN){
                                Questionnaire eval = QuestionnaireService.getInstance().getOrNewQuestionnaireChronique();
                                Questionnaire lombalgie = QuestionnaireService.getInstance().getOrNewQuestionnaireLombalgie();

                                lombalgie.getModificateursQuestion().add(UtilisateurEsterService.getInstance().readByEmailOrId(user.getIdentifiant()));
                                eval.getModificateursQuestion().add(UtilisateurEsterService.getInstance().readByEmailOrId(user.getIdentifiant()));
                                user.getQuestionnairesAcces().add(lombalgie);
                                user.getQuestionnairesAcces().add(eval);

                                QuestionnaireService.getInstance().save(eval);
                                QuestionnaireService.getInstance().save(lombalgie);
                                UtilisateurEsterService.getInstance().save(user);
                                log.info("Utilisateur questionnaire : " + UtilisateurEsterService.getInstance().readByEmailOrId(user.getEmail()).getQuestionnairesAcces());
                            }

                            //On redirige l'utilisateur vers la page de connexion
                            request.getSession().invalidate();
                            vue = "index.jsp";

                    } else {
                        String message = "Les mots de passe ne sont pas identiques.";
                        Servlet.ATT_MSG_WARNING.write(message, request);
                    }
                }
            }
        }
        Servlet.dispatch(request, response, vue);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        log.info("Do get ResetPasswordServlet");
        Servlet.dispatch(request, response, VUE_NEW_ACCOUNT_USER);
    }
}
