<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.formulaire.SalarieForm"%>
<%@page import="fr.univangers.esterwebapp.model.modelenum.utilisateur.Sexe"%>
<%@page import="fr.univangers.esterwebapp.formulaire.connexion.ParametresConnexionModelForm"%>
<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>

<div class="row m-2" id="container_formpatient">
    <div class="col justify-content-center">
        <div class="row justify-content-center">
            <div class="col-md-auto">
                <h4 class="font-weight-normal text-center">Veuillez saisir vos informations</h4>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <form method="post">
                    <c:if test='${not empty param[Servlet.PARAM_PAGE]}'>
                        <input type="hidden" name="${Servlet.PARAM_PAGE}" value="${param[Servlet.PARAM_PAGE]}" />
                    </c:if>
                    <c:if test='${not empty utilisateur}'>
                        <input type="hidden" name="${ParametresConnexionModelForm.UTILISATEUR_SELECT}" value="${utilisateur.identifiant}" />
                    </c:if>

                    <div class="form-group row">
                        <label class="col-sm-3">Sexe</label>
                        <div class="col-sm-9">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="${SalarieForm.SEXE_INPUT}" id="${SalarieForm.SEXE_INPUT_H}" value="${Sexe.HOMME.code}" checked>
                                <label class="form-check-label" for="${SalarieForm.SEXE_INPUT}">${Sexe.HOMME.nom}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="${SalarieForm.SEXE_INPUT}" id="${SalarieForm.SEXE_INPUT_F}" value="${Sexe.FEMME.code}">
                                <label class="form-check-label" for="${SalarieForm.SEXE_INPUT}">${Sexe.FEMME.nom}</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="${SalarieForm.AGE_SELECT}">Âge</label>
                        <div class="col-sm-9">
                            <select name="${SalarieForm.AGE_SELECT}" id="${SalarieForm.AGE_SELECT}" class="custom-select">
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="${SalarieForm.REGION_INPUT}">Région d'activité</label>
                        <div class="col-sm-9">
                            <select name="${SalarieForm.REGION_INPUT}" id="${SalarieForm.REGION_INPUT}" class="custom-select">
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="${SalarieForm.DEPARTEMENT_INPUT}">Département d’activité</label>
                        <div class="col-sm-9">
                            <select name="${SalarieForm.DEPARTEMENT_INPUT}" id="${SalarieForm.DEPARTEMENT_INPUT}" class="custom-select">
                            </select>
                        </div>
                    </div>



                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="${SalarieForm.CODE_NAF_INPUT}">NAF</label>
                        <div class="col-sm-9">
                            <select name="${SalarieForm.CODE_NAF_INPUT}" id="${SalarieForm.CODE_NAF_INPUT}" class="custom-select">
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="${SalarieForm.CODE_PCS_INPUT}">PCS</label>
                        <div class="col-sm-9">
                            <select name="${SalarieForm.CODE_PCS_INPUT}" id="${SalarieForm.CODE_PCS_INPUT}" class="custom-select">
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="${SalarieForm.ENTREPRISE_INPUT}">Entreprise/Administration</label>
                        <div class="col-sm-9">
                            <select name="${SalarieForm.ENTREPRISE_INPUT}" id="${SalarieForm.ENTREPRISE_INPUT}" class="custom-select">
                                <c:forEach items="${entreprise}" var="entreprise">
                                    <option value="${entreprise.identifiant}">${entreprise.nom}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>


            <div class="from-group row d-flex justify-content-center">
                        <div class="col-md-12 row d-flex justify-content-center">
                            <button type="submit" class="col-md-2 btn btn-md btn-primary d-flex justify-content-center mr-2" id="load">Valider</button>
                            <button class="btn btn-block btn-danger col-md-2 btn-md d-flex justify-content-center" type="reset">Annuler</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<c:url value="/public/js/ownFiles/populate_select.js"/>"></script>

<script>
    populateSelect("<c:url value="/public/json/proposition/Age.json"/>","${SalarieForm.AGE_SELECT}","${empty utilisateur.age.code ? "" : utilisateur.age.code}");
    populateSelect("<c:url value="/public/json/proposition/Region.json"/>","${SalarieForm.REGION_INPUT}","${empty utilisateur.travail.regionActivite.code ? "" : utilisateur.travail.regionActivite.code}");
    populateSelect("<c:url value="/public/json/proposition/Departement.json"/>","${SalarieForm.DEPARTEMENT_INPUT}","${empty utilisateur.travail.departementActivite.code ? "" : utilisateur.travail.departementActivite.code}");
    populateSelect("<c:url value="/public/json/proposition/NAF.json"/>","${SalarieForm.CODE_NAF_INPUT}","${empty utilisateur.travail.codeNAF.code ? "" : utilisateur.travail.codeNAF.code}");
    populateSelect("<c:url value="/public/json/proposition/PCS.json"/>","${SalarieForm.CODE_PCS_INPUT}","${empty utilisateur.travail.codePCS.code ? "" : utilisateur.travail.codePCS.code}");

    document.getElementById("${SalarieForm.REGION_INPUT}").addEventListener('change', function () {
        populateDep("${SalarieForm.REGION_INPUT}", "${SalarieForm.DEPARTEMENT_INPUT}")
    });
    document.getElementById("${SalarieForm.DEPARTEMENT_INPUT}").addEventListener('change', function () {
        populateReg("${SalarieForm.DEPARTEMENT_INPUT}", "${SalarieForm.REGION_INPUT}")
    });

</script>
