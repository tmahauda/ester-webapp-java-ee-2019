<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.model.modelenum.utilisateur.Role"%>
<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.formulaire.connexion.ParametresConnexionCollectionForm" %>
<%@page import="fr.univangers.esterwebapp.formulaire.connexion.ParametresConnexionModelForm"%>
<%@page import="fr.univangers.esterwebapp.formulaire.SalarieForm"%>

<c:choose>

    <%--Si ce n'est pas un salarié alors modif uniquement des paramétres de connexion--%>
    <c:when test="${utilisateur.role != Role.SALARIE}">

            <form id="parametresConnexion" class="container-fluid row" method="post">

                <div class="row col-md-12 d-flex justify-content-center">
                    <h3>Modification des paramètres de compte : </h3>
                </div>

                <c:if test='${not empty param[Servlet.PARAM_PAGE]}'>
                    <input type="hidden" name="${Servlet.PARAM_PAGE}" value="${param[Servlet.PARAM_PAGE]}" />
                </c:if>
                <c:if test='${not empty param[ParametresConnexionCollectionForm.TYPE_UTILISATEUR]}'>
                    <input type="hidden" name="${ParametresConnexionCollectionForm.TYPE_UTILISATEUR}" value="${param[ParametresConnexionCollectionForm.TYPE_UTILISATEUR]}" />
                </c:if>
                <c:if test='${not empty utilisateur}'>
                    <input type="hidden" name="${ParametresConnexionModelForm.UTILISATEUR_SELECT}" value="${utilisateur.identifiant}" />
                </c:if>

                <div class="row col-md-12 d-flex justify-content-center">
                    <div class="form-label-group">
                        <label for="${ParametresConnexionModelForm.NEW_IDENTIFIANT_INPUT}">Identifiant</label>
                        <input type="text" id="${ParametresConnexionModelForm.NEW_IDENTIFIANT_INPUT}" name="${ParametresConnexionModelForm.NEW_IDENTIFIANT_INPUT}" value="${utilisateur.identifiant}" class="form-control" required>
                        <div class="col pb-4"></div>
                    </div>
                </div>

                <div class="row col-md-12 d-flex justify-content-center">
                    <div class="form-label-group">
                        <label for="${ParametresConnexionModelForm.EMAIL_INPUT}">Email</label>
                        <input type="email" id="${ParametresConnexionModelForm.EMAIL_INPUT}" name="${ParametresConnexionModelForm.EMAIL_INPUT}" value="${utilisateur.email}" class="form-control" required>
                        <div class="col pb-4"></div>
                    </div>
                </div>

                <div class="row col-md-12 d-flex justify-content-center">
                    <div class="form-label-group">
                        <label for="${ParametresConnexionModelForm.OLD_MOTPASSE_INPUT}">Mot de passe actuel</label>
                        <input type="password" id="${ParametresConnexionModelForm.OLD_MOTPASSE_INPUT}" name="${ParametresConnexionModelForm.OLD_MOTPASSE_INPUT}" class="form-control">
                        <div class="col pb-4"></div>
                    </div>
                </div>

                <div class="row col-md-12 d-flex justify-content-center">
                    <div class="form-label-group">
                        <label for="${ParametresConnexionModelForm.NEW_MOTPASSE_INPUT}">Nouveau mot de passe</label>
                        <input type="password" id="${ParametresConnexionModelForm.NEW_MOTPASSE_INPUT}" name="${ParametresConnexionModelForm.NEW_MOTPASSE_INPUT}" class="form-control" onkeyup="return passwordChanged('${ParametresConnexionModelForm.NEW_MOTPASSE_INPUT}');"  onBlur="checkPass('${ParametresConnexionModelForm.NEW_MOTPASSE_INPUT}','inputNew2Password')">
                        <span id="strength" class="col col-xs-2"></span>
                    </div>
                </div>

                <div class="row col-md-12 d-flex justify-content-center">
                    <div class="form-label-group">
                        <label for="inputNew2Password">Confirmer le mot de passe</label>
                        <input type="password" id="inputNew2Password" name="confirm" class="form-control" onkeyup="checkPass('${ParametresConnexionModelForm.NEW_MOTPASSE_INPUT}','inputNew2Password')">
                    </div>
                </div>

                <div id="divcomp">
                    <div class="pt-2" >

                    </div>
                </div>

                        <div class="col-md-12 row d-flex justify-content-center mt-4">
                            <input type="submit" class="col-md-2 btn btn-md btn-primary d-flex justify-content-center mr-2" value="Valider" id="submit">
                            <input type="reset" class="btn btn-danger col-md-2 btn-md d-flex justify-content-center" value="Annuler">
                        </div>
            </form>

    </c:when>

    <c:otherwise>
        <c:import url="${SalarieForm.VUE_FORM_SALARIE}"/>
    </c:otherwise>

</c:choose>