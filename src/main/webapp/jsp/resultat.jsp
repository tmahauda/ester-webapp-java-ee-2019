<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">

    <title>Connexion - ESTER</title>

    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/Accueil.css"/>">

    <script src="<c:url value="/dwr/engine.js"/>"></script>
    <script src="<c:url value="/dwr/interface/Resultat.js"/>"></script>
    <script src="<c:url value="/dwr/util.js"/>"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/public/js/ownFiles/resultat.js"/>"></script>
    <script>
        function showDiv(name) {
            document.getElementById('divSalarie').style.display = "none";
            document.getElementById('divEntreprise').style.display = "none";
            document.getElementById('divUtilisateur').style.display = "none";
            document.getElementById(name).style.display = "block";
        }

        function timer(lien) {
            window.setTimeout(function () {
                document.location.href="<%=request.getContextPath()%>/" + lien;
            }, 1000);
        }
    </script>

</head>
<body class="d-flex flex-column h-100">

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>

<main>
    <div class="row">
        <div class="col-md-4 pl-4">
            <form  id="saisi" method="post">

                <label for="listeSalaries">Choisissez un Salarié :</label>

                <div class="row pt-2 pb-4">
                    <div class="col-md-8">
                        <select class="form-control" name="identifiantSalarie" id="listeSalaries">
                            <c:forEach items="${listSalarie}" var="identifiant" >
                                <option> <c:out value="${identifiant}"></c:out> </option>
                            </c:forEach>
                            <c:remove var="listSalarie" scope="session" />
                        </select>
                    </div>

                    <div class="col-md-2">
                        <input type="submit" class="btn btn-info" value="Valider" style="float:left;">
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="card-group">

        <!-- Histogramme -->

        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Situation du score de l'histogramme de la population de référence</h5>
            </div>
            <div id="histo-div" class="card-body">
                <div id="id_histo" style="min-width: 310px; max-width: 1000px; margin: 0 auto"></div>
            </div>
        </div>

        <!-- Comparaison -->

        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Comparaison des réponses à la population de référence* (Hommes de 50 ans ou plus, B=396)</h5>
                <h6 class="card-subtitle mb-2 text-muted">*Population de référence : échantillion du réseaux de surveillance des TMS en entreprise dans les Pays de la Loire (2002-2005)</h6>
            </div>
            <div id="chart-div" class="card-body">
            </div>
        </div>

        <!-- RPE -->

        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Intensité des efforts physiques (échelle Rating Scale of Perceived Extertion (RPE) de Borg)</h5>
            </div>
            <div id="strong-div" class="card-body">
                <div id="id_rpe" style="min-width: 310px; max-width: 1000px; margin: 0 auto"></div>
            </div>
        </div>

    </div>
    <script>
        addHisto("id_histo");
        addAllChart("chart-div");
        addRPE("id_rpe");
    </script>
</main>

<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>

</body>
</html>
