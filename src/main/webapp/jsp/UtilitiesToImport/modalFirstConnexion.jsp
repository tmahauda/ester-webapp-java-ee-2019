<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal" role="dialog" id="modalAccueil">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informations relatives aux données utilisateurs</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <c:import url="/jsp/UtilitiesToImport/DataAccordeon.jsp"/>
                    <b>Vous pouvez retrouver toutes ces informations dans les "Mentions Légales" en bas de page.</b></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="AgreeButton">Accepter</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="DisagreeButton">Refuser</button>

            </div>
        </div>
    </div>
</div>

<div class="modal" role="dialog" id="modalVerif">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Vérification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><u>Êtes-vous vraiment sûr de ne pas vouloir nous partager ces informations ?</u> <br>
                    Votre refus ne sera pas définitif mais vous retournera vers une page ultérieur.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="Retour">Oui</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="ModalAgain">Non</button>

            </div>
        </div>
    </div>
</div>

<script src="<c:url value="/public/js/ownFiles/modalsFirstConnexion.js"/>"></script>


