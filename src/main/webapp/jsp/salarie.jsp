<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.formulaire.SalarieForm"%>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Page salarié - ESTER</title>

    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">

    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>
    <link rel="stylesheet" href="<c:url value="/public/css/global_style.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/menu.css"/>">

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/jsp/UtilitiesToImport/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">

            <!-- Menu pour salarié -->

            <div class="col-md-2 menu_gauche" style="background: #158CBA;">
                <ul style="color:white; padding:15px;">
                    <li>
                        <a style="color:white;" href="<c:url value="/salarie?page=modifierProfil"/>">Modifier mon profil</a>
                    </li>
                    <li>
                        <a style="color:white;" href="<c:url value="/salarie/questionnaire"/>">Voir les questionnaires</a>
                    </li>
                    <!--
                    <li>
                        <a style="color:white;" href="<c:url value="/resultat"/>">Voir les statistiques</a>
                    </li>
                    -->
                </ul>
            </div>


            <!-- Container du contenu du salarié -->

                <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                    <div class="card-body center_block">
                        <c:if test="${not empty Message && Message}">
                            <c:import url="${Servlet.VUE_ALERT}"/>
                        </c:if>

                        <h1>Salarié</h1>
                        <p>Bienvenue sur la partie Salarié du Projet ESTER</p>
                        <div style="text-align: initial;">

                        <c:if test="${(not empty Utilisateur && Utilisateur.firstConnexion) || param.page == 'modifierProfil'}">
                            <c:import url="${SalarieForm.VUE_FORM_SALARIE}"/>
                        </c:if>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/jsp/UtilitiesToImport/footer.jsp"/>

</body>
</html>