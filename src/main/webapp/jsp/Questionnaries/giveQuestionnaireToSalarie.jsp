<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.Questionnaires.GiveQuestionnaireToSalarieServlet"%>

<div class="row m-2" id="container">
    <div class="col justify-content-center">
        <div class="row justify-content-center">
            <div class="col-md-auto">
                <h4 class="font-weight-normal text-center">Affecter un questionnaire à un salarié</h4>
            </div>
        </div>

        <div class="row">

        <form class="col-md-12" method="get">

            <c:if test='${not empty GiveQuestionnaireToSalarieServlet.URL_GIVE_QUESTIONNAIRE_TO_SALARIE}'>
                <input type="hidden" name="${Servlet.PARAM_PAGE}" value="${GiveQuestionnaireToSalarieServlet.URL_GIVE_QUESTIONNAIRE_TO_SALARIE}" />
            </c:if>



            <c:if test="${not empty salaries}">
            <div class="form-group row">
                    <div class="col-md-10">
                        <label for="${GiveQuestionnaireToSalarieServlet.SALARIE_SELECT}">Sélectionner un salarié</label>
                        <select id="${GiveQuestionnaireToSalarieServlet.SALARIE_SELECT}" name="${GiveQuestionnaireToSalarieServlet.SALARIE_SELECT}" class="custom-select">
                            <c:forEach items="${salaries}" var="salarie">
                                <option value="${salarie.identifiant}" <c:if test="${param[GiveQuestionnaireToSalarieServlet.SALARIE_SELECT]==salarie.identifiant}">selected</c:if>>${salarie.identifiant}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="col-md-2 d-flex mt-auto">
                        <button class="btn btn-block btn-primary" type="submit">Sélectionner</button>
                    </div>
            </div>
            </c:if>

        </form>
        </div>
<div class="row">
        <form class="col-md-12" method="post">

            <c:if test='${not empty param[GiveQuestionnaireToSalarieServlet.QUESTIONNAIRE_SELECT]}'>
                <input type="hidden" name="${GiveQuestionnaireToSalarieServlet.QUESTIONNAIRE_SELECT}" value="${param[GiveQuestionnaireToSalarieServlet.QUESTIONNAIRE_SELECT]}" />
            </c:if>

            <c:if test="${not empty questionnaires}">

                <div class="form-group row">
                    <div class="col-md-10">
                    <label for="${GiveQuestionnaireToSalarieServlet.QUESTIONNAIRE_SELECT}">Sélectionner un questionnaire auquel lui affecter</label>
                    <select id="${GiveQuestionnaireToSalarieServlet.QUESTIONNAIRE_SELECT}" name="${GiveQuestionnaireToSalarieServlet.QUESTIONNAIRE_SELECT}" class="custom-select">
                        <c:forEach items="${questionnaires}" var="questionnaire">
                            <option value="${questionnaire.identifiant}">${questionnaire.nom}</option>
                        </c:forEach>
                    </select>
                    </div>

                    <div class="col-md-2 d-flex mt-auto">
                        <button type="submit" class="btn btn-md btn-primary" id="load">Attribuer le questionnaire</button>
                    </div>
                </div>
            </c:if>

        </form>
</div>
    </div>
</div>