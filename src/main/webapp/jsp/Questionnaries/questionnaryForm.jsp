<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>

<script src="https://kit.fontawesome.com/9dc80aad96.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="<c:url value="/public/css/ownFiles/questionnary.css"/>">

<c:if test="${not empty Message && Message}">
    <c:import url="${Servlet.VUE_ALERT}"/>
</c:if>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 containerQuestionnary" style="background-color: #158CBA">
            <div class="upperButtonList">
                <div class="row d-flex align-middle">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <button class="btn btn-lg btn-success d-flex" id="popupQuestionnaryList">Liste des questionnaires</button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <button class="btn btn-lg btn-outline-light customMarginSelect" disabled id="popupSharePrivilege">Partager les droits</button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group customMarginSelect">
                            <select class="form-control custom-select onFormDisabled" id="selectQuestionnaryAction">
                                <option value="0" selected disabled>Gérer les questionnaires</option>
                                <option value="1">Créer un questionnaire</option>
                                <option value="2" disabled >Dupliquer un questionnaire</option>
                                <option value="3" disabled >Modifier un questionnaire</option>
                                <option value="4" disabled >Supprimer un questionnaire</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="containerInsideQuestionnary">
                <div class="row m-2" >
                    <div class="col-md-11">
                        <div id="questionnaryNameContainer" style="margin-top:15px">
                        </div>
                    </div>
                    <div class="col-md-1">
                        <a class="pullRight" id="popupAddQuestion" title="Ajouter une question">&nbsp;<em class="fas fa-plus fa-3x"></em></a>
                    </div>
                </div>

                <div id="displayedQuestionnaryQuestionList" class="container-fluid fixedMaxSizeQuestion m-2">
                </div>
            </div>
            <div class="onFormShowed mb-4" style="background-color:#FFF; height: 60px;">
                <div class="row mt-2 d-flex justify-content-center">
                    <button class="col-md-2 btn btn-md btn-success d-flex mt-1 mr-1 ml-2 justify-content-center" id="saveTempo" type="button">Sauvegarde temporaire </button>
                    <button class="col-md-2 btn btn-md btn-primary d-flex mt-1 mr-1 ml-2 justify-content-center" id="validateAction" type="submit">Valider </button>
                    <button class="col-md-2 btn btn-md btn-warning d-flex mt-1 mr-1 ml-2 justify-content-center" id="resetAction">Reset</button>
                    <button class="col-md-2 btn btn-md btn-danger d-flex  mt-1 mr-1 ml-2 justify-content-center" id="cancelAction">Annuler</button>
                </div>
            </div>
        </div>
    </div>

</div>

<c:import url="/jsp/Questionnaries/modalsQuestionnaries.jsp"/>

<script src="<c:url value="/public/js/ownFiles/questionnaryManager.js"/>"></script>
<script src="<c:url value="/public/js/libraries/jquery.session.js "/>"></script>

