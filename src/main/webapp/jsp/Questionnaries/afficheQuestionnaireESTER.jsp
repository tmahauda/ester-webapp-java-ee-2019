<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page import="fr.univangers.esterwebapp.servlet.Servlet"%>
<%@page import="fr.univangers.esterwebapp.servlet.Questionnaires.AfficheQuestionnaireESTER"%>
<c:set var="QuestionnairesSelect" value="${AfficheQuestionnaireESTER.QUESTIONNAIRES_SELECT}" scope="request"/>


                <div class="container-fluid mt-3" id="container">

                    <h1>Questionnaire</h1>

                    <!-- Tous les questionnaires non répondus du salarié -->
                    <div id="listQuestionnaire">
                        <form method="get">
                            <div class="form-group row">
                                <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                    <label for="${QuestionnairesSelect}">Liste des Questionnaires : </label>
                                </div>
                                <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                    <select id="${QuestionnairesSelect}" name="${QuestionnairesSelect}" class="custom-select">
                                        <c:forEach items="${questionnaires}" var="questionnaire">
                                            <option value="${questionnaire.identifiant}">${questionnaire.nom}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <input type="hidden" name="${Servlet.PARAM_PAGE}" value="${AfficheQuestionnaireESTER.URL_AFFICHE_QUESTIONNAIRE_ESTER}">

                                <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                    <button class="btn btn-primary btn-md" type="submit">Choisir</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <c:if test="${not empty Questionnaire}">

                        <!-- Questionnaire sélectionné -->
                        <div class="row m-2 justify-content-center">
                            <div class="col justify-content-center">
                                <div class="row justify-content-center">
                                    <div class="col-md-auto my-3" id="titreQuestionnaire">
                                    </div>
                                </div>

                                <div class="row justify-content-center">
                                    <div class="col md-auto justify-content-center">
                                            <!-- Questions -->
                                        <div id="containerQuestionnaire" class="container-fluid row d-flex justify-content-center">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script src="<c:url value="/public/js/ownFiles/afficheQuestionnaire.js"/>"></script>

                        <script>
                            affiche('${Questionnaire}','idQuestion');
                        </script>

                    </c:if>

                </div>
            </div>
        </div>


