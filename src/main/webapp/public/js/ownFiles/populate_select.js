function populateSelect(file, select, currentValueCode) {

    var stringSelectName = "";
    if(file.includes("/public/json/proposition/Age.json")){
        stringSelectName = "Sélectionner un âge";
    }
    else if(file.includes("/public/json/proposition/Region.json")){
        stringSelectName = "Sélectionner une région";
    }
    else if(file.includes("/public/json/proposition/Departement.json")){
        stringSelectName = "Sélectionner un département";
    }
    else if(file.includes("/public/json/proposition/NAF.json")){
        stringSelectName = "Sélectionner une valeur NAF";
    }
    else if(file.includes("/public/json/proposition/PCS.json")){
        stringSelectName = "Sélectionner une valeur PCS";
    }
    else{
            console.log("Error : file unknown !");
    }

    //On met une valeur vide par défaut
    if(currentValueCode == "") {
        //On selectionne l'option
        document.getElementById(select).innerHTML += "<option disabled selected value=\"\">"+stringSelectName+"</option>";
    } else {
        //On ne sélectionne pas l'option
        document.getElementById(select).innerHTML += "<option disabled value=\"\">"+ stringSelectName+"</option>";
    }

    //On peuple le select avec le fichier json
    $.getJSON(file, function( json ) {
        $.each(json, function(i) {
            if(file.includes("/public/json/proposition/Age.json") || file.includes("/public/json/proposition/Region.json")){
                if(currentValueCode == json[i].code) {
                    //On selectionne l'option
                    document.getElementById(select).innerHTML += "<option selected value=\"" + json[i].code + "\">"+ json[i].nom + "</option>";
                } else {
                    //On ne sélectionne pas l'option
                    document.getElementById(select).innerHTML += "<option value=\"" + json[i].code+"\">" + json[i].nom + "</option>";
                }
            }
            else {
                if(currentValueCode == json[i].code) {
                    //On selectionne l'option
                    document.getElementById(select).innerHTML += "<option selected value=\"" + json[i].code + "\">"+ json[i].code+" "+ json[i].nom + "</option>";
                } else {
                    //On ne sélectionne pas l'option
                    document.getElementById(select).innerHTML += "<option value=\"" + json[i].code+"\">" + json[i].code+" "+ json[i].nom + "</option>";
                }
            }

        });
    });
}

function populateDep(s1,s2){
    document.getElementById(s2).innerHTML = "<option value=\"\"> </option>";
    var option;

    $.getJSON("public/json/proposition/Region.json", function( json ) {
        $.each(json, function(i) {
            if(json[i].code == document.getElementById(s1).value){
                option=json[i].code;
            }
        });
    });

    $.getJSON("public/json/proposition/Departement.json", function( json ) {
        $.each(json, function(i) {
            if(scoreDeptToCodeDept(json[i].score) == option){
                document.getElementById(s2).innerHTML += "<option value=\"" + json[i].code + "\">" + json[i].code+" " + json[i].nom + "</option>";
            }
        });
    });

}

function populateReg(s1,s2){
    document.getElementById(s2).innerHTML = "<option value=\"\"> </option>";
    var option;

    $.getJSON("public/json/proposition/Departement.json", function( json ) {
        $.each(json, function(i) {
            if(json[i].code == document.getElementById(s1).value){
                option = scoreDeptToCodeDept(json[i].score);
            }
        });
    });

    $.getJSON("public/json/proposition/Region.json", function( json ) {
        $.each(json, function(i) {
            if(json[i].code == option){
                document.getElementById(s2).innerHTML += "<option selected value=\"" + json[i].code + "\">" + json[i].nom + "</option>";
            } else {
                document.getElementById(s2).innerHTML += "<option value=\"" + json[i].code + "\">" + json[i].nom + "</option>";
            }
        });
    });

}

function scoreDeptToCodeDept(score) {
    if(score == 0) return "COM";
    else return score > 9 ? score.toString() : "0" + score.toString();
}