var numQuestion = 0;

function affiche(objetJSON,nameQuestion, nameReponse, nameNbQuestions) {

    if(nameReponse == undefined && nameNbQuestions== undefined) {
        afficheEtRepond(objetJSON,nameQuestion, null, null);
    }
    else {
        afficheEtRepond(objetJSON, nameQuestion, nameReponse, nameNbQuestions);
    }
}


function afficheEtRepond(objetJSON, nameQuestion, nameReponse, nameNbQuestions) {
    var objQuestionnaire = JSON.parse(objetJSON);
    nameQuestionnaire = objQuestionnaire.nom;
    console.log(nameQuestionnaire);

    // Affichage du titre du questionnaire
    var titre = document.createElement("h3");
    titre.setAttribute("class","font-weight-normal text-center");
    var txtTitre = document.createTextNode(objQuestionnaire.nom);
    titre.appendChild(txtTitre);
    document.getElementById("titreQuestionnaire").appendChild(titre);

    var container = document.getElementById("containerQuestionnaire");

    // On place dans un input invisible le nom du questionnaire pour enregistrer les réponses dans la base
    var donneesQuestionnaire = document.createElement("input");
    donneesQuestionnaire.setAttribute("type","hidden");
    donneesQuestionnaire.setAttribute("name","nameQuestionnaire");
    donneesQuestionnaire.setAttribute("value",nameQuestionnaire);
    container.appendChild(donneesQuestionnaire);

    /* Types de questions :
        shortAnswer
        longAnswer
        checkbox
        radio
        picture
        video
    */

    var tabQuestions = objQuestionnaire.questions;

    // On parcourt la collection de questions pour les trainer une à une
    // Toutes les questions sont en "required", puisque logiquement la personne doit
    for(var i=0; i<tabQuestions.length; i++) {

        // Chaque question est dans un <div class="from-group">
        var form_grp = document.createElement("div");
        form_grp.setAttribute("class", "form-group container-fluid col-md-12 mb-4 d-flex justify-content-center row");

        var question = tabQuestions[i];

        // On récupère le type et on place l'intitule de la question
        var type = question.typeQuestion;

        // On récupère l'intitulé de la question que l'on place dans un label (sauf si c'est une image ou une vidéo)
        if (type !== "picture" && type !== "video") {
            numQuestion++;

            var hidden = document.createElement("input");
            hidden.setAttribute("type", "hidden");
            hidden.setAttribute("name", "Question"+numQuestion);
            hidden.setAttribute("value", question.intituleQuestion);
            hidden.setAttribute("class", "row");
            form_grp.appendChild(hidden);

            var labelRow = document.createElement("div");
            labelRow.setAttribute("class", "row");
            var label = document.createElement("label");
            label.setAttribute("class", "col-md-12");
            var underline = document.createElement("u");

            var intitule = document.createTextNode(question.intituleQuestion);

            underline.appendChild(intitule);
            label.appendChild(document.createTextNode(numQuestion + ") "));
            label.appendChild(underline);

            labelRow.appendChild(label);

            form_grp.appendChild(label);
        }

        // Si une question de type radio a 6 réponses ou plus, elle passe en type select pour un affichage de meilleure qualité
        if (type === "radio" && question.optionsQuestion.length >= 6) {
            type = "select";
        }

        // S'il s'agit d'un type input_text, le tableau de réponses est vide
        if (type === "shortAnswer") {

            var row1 = document.createElement("div");
            row1.setAttribute("class", "col-md-10 justify-content-center d-flex");

            var input_txt = document.createElement("input");
            input_txt.setAttribute("class", "form-control questionWritten");
            input_txt.setAttribute("type", "text");

            if(nameReponse == undefined && nameNbQuestions== undefined) {
                input_txt.setAttribute("disabled", "true");
            }
            else{
                $(input_txt).prop("required", "required");
            }

            var txtId1 = "q_" + numQuestion;
            input_txt.setAttribute("id", txtId1);
            input_txt.setAttribute("name", txtId1);

            row1.appendChild(input_txt);
            form_grp.appendChild(row1);

        }

        // Même chose pour un textarea
        if (type === "longAnswer") {

            var row2 = document.createElement("div");
            row2.setAttribute("class", "col-md-10 justify-content-center d-flex");


            var textarea = document.createElement("textarea");
            textarea.setAttribute("class", "form-control questionWritten");
            if(nameReponse == undefined && nameNbQuestions== undefined) {
                textarea.setAttribute("disabled", "true");
            }
            else{
                $(textarea).prop("required", "required");
            }

            textarea.setAttribute("rows", "3");
            var txtId2 = "q_" + numQuestion;
            textarea.setAttribute("id", txtId2);
            textarea.setAttribute("name", txtId2);

            row2.appendChild(textarea);
            form_grp.appendChild(row2);

        }

        // S'il s'agit d'un type select, les réponses s'affichent dans un menu déroulant
        if (type === "select") {


            var col = document.createElement("div");
            col.setAttribute("class", "col-md-10");

            var select = document.createElement("select");
            select.setAttribute("class", "custom-select questionSelected");

            if(nameReponse != undefined || nameNbQuestions!= undefined) {
                $(select).prop("required", "required");
                select.setAttribute("name", nameReponse + numQuestion);
            }

            var option_vide = document.createElement("option");
            option_vide.setAttribute("selected", "true");
            option_vide.setAttribute("class","emptyOption");
            select.appendChild(option_vide);

            var num_option = 0;

            // On affiche les réponses une par une sous forme de bouton radio
            for (var j = 0; j < question.optionsQuestion.length; j++) {

                var opt = question.optionsQuestion[j];

                num_option++;
                var select_option = document.createElement("option");
                var txtId3 = "q_" + numQuestion + "_r_" + (num_option);
                select_option.setAttribute("id", txtId3);
                if(nameReponse == undefined && nameNbQuestions== undefined) {
                    select_option.setAttribute("disabled", "true");
                }

                var option1 = document.createTextNode(opt.valeur);

                select_option.appendChild(option1);
                select.appendChild(select_option);
            }

            col.appendChild(select);
            form_grp.appendChild(col);
        }

        // S'il s'agit d'un type radio, on doit choisir une seule réponse parmi plusieurs
        if (type === "radio") {

            var num_radio = 0;

            // On affiche les réponses une par une sous forme de bouton radio
            for (var k = 0; k < question.optionsQuestion.length; k++) {

                var optRadio = question.optionsQuestion[k];
                num_radio++;

                // Chaque option est dans un <div class="form-check">

                var col_rad = document.createElement("div");
                col_rad.setAttribute('class',"col-md-10");

                var form_chk1 = document.createElement("div");
                form_chk1.setAttribute("class", "form-check");

                var input_radio = document.createElement("input");
                input_radio.setAttribute("class", "form-check-input reponseQCM");
                input_radio.setAttribute("type", "radio");

                if(nameReponse == undefined && nameNbQuestions== undefined) {
                    input_radio.setAttribute("disabled", "true");
                }
                else{
                    $(input_radio).prop("required", "required");
                    input_radio.setAttribute("name", nameReponse + numQuestion);
                    input_radio.setAttribute("value", optRadio.valeur);
                }
                var txtId4 = "q_" + numQuestion + "_r_" + num_radio;
                input_radio.setAttribute("id", txtId4);

                label = document.createElement("label");
                label.setAttribute("class", "form-check-label pt-1 ml-4");
                label.setAttribute("for", txtId4);
                var option2 = document.createTextNode(optRadio.valeur);
                label.appendChild(option2);

                form_chk1.appendChild(input_radio);
                form_chk1.appendChild(label);
                col_rad.appendChild(form_chk1);

                form_grp.appendChild(col_rad);
            }
        }

        // S'il s'agit d'un type checkbox, on peux choisir plusieurs réponses parmi celles proposées
        if (type === "checkbox") {

            // On affiche les réponses une par une sous forme de bouton radio
            for (var l = 0; l < question.optionsQuestion.length; l++) {

                // Chaque option est dans un <div class="form-check">

                var col_chk = document.createElement("div");
                col_chk.setAttribute('class',"col-md-10");

                var form_chk = document.createElement("div");
                form_chk.setAttribute("class", "form-check");

                var checkbox = document.createElement("input");
                checkbox.setAttribute("class", "form-check-input reponseQCM");
                checkbox.setAttribute("type", "checkbox");
                checkbox.setAttribute("name", "q_" + numQuestion);
                var txtId = "q_" + numQuestion + "_r_" + (l + 1);
                checkbox.setAttribute("id", txtId);
                if(nameReponse == undefined && nameNbQuestions== undefined) {
                    checkbox.setAttribute("disabled", "true");
                }


                label = document.createElement("label");
                label.setAttribute("class", "form-check-label pt-1 ml-4");
                label.setAttribute("for", txtId);
                var option = document.createTextNode(question.optionsQuestion[l].valeur);
                label.appendChild(option);

                form_chk.appendChild(checkbox);
                form_chk.appendChild(label);

                col_chk.appendChild(form_chk);

                form_grp.appendChild(col_chk);

            }
        }


        // S'il s'agit d'un type image, le tableau contient un lien vers l'image à afficher sous la question
        /*if(type==="picture"){

            var instruction = document.createElement("label");
            var txt = document.createTextNode("[type : image] Observez cette image pour répondre à la question "+(numQuestion+1)+" :");
            instruction.appendChild(txt);
            form_grp.appendChild(instruction);

            var row = document.createElement("div");
            row.setAttribute("class","row");

            var col = document.createElement("div");
            col.setAttribute("class","col col-md-auto");

            var img = document.createElement("img");
            img.setAttribute("src",reponses[0].lien);
            img.setAttribute("class","img-fluid");
            img.setAttribute("alt",reponses[0].lien);

            col.appendChild(img);
            row.appendChild(col);
            form_grp.appendChild(row);

         }*/

        // Type vidéo où celle-ci est incluse dans l'affichage
        /** ATTENTION
         * Cet affichage fonctionne uniquement avec un lien youtube de type "https://ww.youtube.com/embed/..."
         * Ce lien s'obtient en cliquant sur "partager" puis "integrer"
         * */
        /*if(type==="video_youtube"){

            var instruction = document.createElement("label");
            var txt = document.createTextNode("Visionnez cette vidéo pour répondre à la question "+(numQuestion+1)+" : (version youtube incluse)");
            instruction.appendChild(txt);
            form_grp.appendChild(instruction);

            var video = document.createElement("div");
            video.setAttribute("class","embed-responsive embed-responsive-16by9");
            var iframe = document.createElement("iframe");
            iframe.setAttribute("class","embed-responsive-item");
            iframe.setAttribute("title","");
            iframe.setAttribute("src",reponses[0].lien);
            iframe.setAttribute("alt",reponses[0].lien);
            iframe.setAttribute("allowfullscreen","true");

            video.appendChild(iframe);
            form_grp.appendChild(video);

        }*/

        // Type vidéo où on affiche juste l'url
        /*if(type==="video"){

            var instruction = document.createElement("label");
            var txt = document.createTextNode("Visionnez cette vidéo pour répondre à la question "+(numQuestion+1)+" : (version url");
            instruction.appendChild(txt);
            form_grp.appendChild(instruction);

            var row = document.createElement("div");
            row.setAttribute("class","row");

            var col = document.createElement("div");
            col.setAttribute("class","col col-md-auto");

            var link = document.createElement("a");
            link.setAttribute("href",reponses[0].lien);
            var txt = document.createTextNode("Cliquez ici");

            link.appendChild(txt);
            col.appendChild(link);
            row.appendChild(col);
            form_grp.appendChild(row);

        }*/
        container.appendChild(form_grp);
    } //fin question

    // Lorsque l'on a fini d'afficher toutes les question, on connait alors leur nombre total
    // On peut donc l'envoyer avec les résultats
    var hidden_nb = document.createElement("input");
    hidden_nb.setAttribute("type","hidden");
    hidden_nb.setAttribute("name", nameNbQuestions);
    hidden_nb.setAttribute("value", numQuestion);

    container.appendChild(hidden_nb);
}

 $(document).on("click","#validateAction", function(){

     $(".reponseQCM").each(function(){
         if($(this).is(":checked")){
             $(this).addClass("requiredOK");
         }
     });
     $(".questionWritten").each(function(){
         if($(this).val() != "" && $(this).val() != undefined) {
             $(this).addClass("requiredOK");
         }
     });
     $(".questionSelected").each(function(){
         var currentSelect = $(this);
         currentSelect.children().each(function(){
             if($(this).is(":selected") && !$(this).hasClass("emptyOption")){
                 currentSelect.addClass("requiredOK");
             }
         })
     });

     var numberOfQuestionsNotEmpty = 0;
     $(document).find('.requiredOK').each(function () {
         numberOfQuestionsNotEmpty+=1;
         $(this).removeClass("requiredOK");
     });

     if(numberOfQuestionsNotEmpty === numQuestion) {
         $('#ModalQuestionnaire').modal('show');
             $('#ModalQuestionnaireOK').on('click', function () {
                 $("#validateAction").prop('type','submit');
                 $("#validateAction").click();
             });
         }
     else {
         $('#MsgErreur').html("Navré mais il vous reste des <u>champs vides</u>. Remplisser les balises vides avant de le valider.");
         $('#ErreurSaisie').modal('show');
     }
});
