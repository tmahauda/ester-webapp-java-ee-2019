$(window).on('load', function () {
    if (document.cookie.indexOf("ModalShown=true") < 0) {
        $('#modalAccueil').modal('show');
    }
});

$('#AgreeButton').click(function() {
    $("#modalAccueil").modal('hide');
    document.cookie = "ModalShown=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
});

$('#DisagreeButton').click(function() {
    $("#modalAccueil").modal('hide');
    $("#modalVerif").modal('show');
});

$('#ModalAgain').click(function() {
    $("#modalVerif").modal('hide');
    $("#modalAccueil").modal('show');
});

$("#Retour").click(function(){
    $("#modalVerif").modal('hide');
    javascript:history.go(-1);
});