# ESTER Webapp

<div align="center">
<img width="400" height="200" src="irset.png">
</div>

## Description du projet

Application web réalisée avec Java EE en MASTER INFO 1 à l'université d'Angers dans le cadre du module "Concrétisation Disciplinaire" durant l'année 2019-2020 avec un groupe de cinq personnes. \
Cette application permet de générer automatiquement des questionnaires liés aux TMS pour les personnels du CHU d'Angers et d'effectuer des analyses statistiques des réponses données par les salariés ESTER.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de cinq étudiants M1 de l'université d'Angers :
- Théo MAHAUDA : tmahauda@etud.univ-angers.fr ;
- Elia MOREAU : emoreau@etud.univ-angers.fr ;
- Souleyman COULIBALY : scoulibaly@etud.univ-angers.fr ;
- Julien DOUET ; jdouet@etud.univ-angers.fr ;
- Luca DAVIS : ldavis@etud.univ-angers.fr.

### Encadrants

Ce projet fut encadré par un groupe de trois étudiants M2 de l'université d'Angers :
- Charles MALLET : cmallet@etud.univ-angers.fr ;
- Imane BELHOUARI : ibelhouari@etud.univ-angers.fr ;
- Théo DEZE : tdeze@etud.univ-angers.fr.

## Organisation

Ce projet a été agit au sein de l'université d'Angers dans le cadre du module "Concrétisation Disciplinaire" du MASTER INFO 1.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2019 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 13/12/2019.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- HTML5 / CSS ;
- JavaScript/JQuery 3 ;
- Bootstrap 4 ;
- Java EE ;
- JUnit 5 ;
- BCrypt ;
- Serveur Apache Tomcat 9 ;
- MongoDB ;
- Hibernate OGM ;
- Intellij IDEA ;
- Gradle ;
- Git.

## Objectifs

Les principales fonctionnalités à intégrer sont :
- Création et gestion des comptes des salariés et professionnels de santé ;
- Génération et administration de questionnaires ;
- Affichage et enregistrement des réponses aux questionnaires ;
- Import et export de données de références ;
- Consultation et analyse des résultats.
